/*
 * @creation 1998-03-31
 * @modification $Date: 2006-09-19 14:42:27 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.curvi;

import org.fudaa.dodico.corba.curvi.IResultatsCurvi;
import org.fudaa.dodico.corba.curvi.IResultatsCurviOperations;
import org.fudaa.dodico.corba.curvi.SResultatsCurviCOR;
import org.fudaa.dodico.corba.curvi.SResultatsCurviELE;
import org.fudaa.dodico.corba.curvi.SResultatsCurviSOL;
import org.fudaa.dodico.corba.curvi.SResultatsLigneCurviCOR;
import org.fudaa.dodico.corba.curvi.SResultatsLigneCurviELE;
import org.fudaa.dodico.corba.curvi.SResultatsLigneCurviSOL;
import org.fudaa.dodico.calcul.DResultats;
import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.fortran.FortranReader;
import java.io.FileReader;

/**
 * La classe d'implatation de IResultatsCurvi gerant les resulatas du code de calcul.
 *
 * @version $Id: DResultatsCurvi.java,v 1.10 2006-09-19 14:42:27 deniger Exp $
 * @author Guillaume Desnoix
 */
public class DResultatsCurvi extends DResultats implements IResultatsCurviOperations, IResultatsCurvi {

  private SResultatsCurviCOR resultsCOR_;
  private SResultatsCurviELE resultsELE_;
  private SResultatsCurviSOL resultsSOL_;

  /**
   * Constructeur vide.
   */
  public DResultatsCurvi() {
    super();
  }

  /**
   * Pas implanter completement.
   *
   * @return <code>new DResultatsCurvi()</code>.
   */
  public final Object clone() throws CloneNotSupportedException {
    return new DResultatsCurvi();
  }

  /**
   * Chaine descriptive de la classe.
   *
   * @return <code>"DResultatsCurvi()"</code>.
   */
  public String toString() {
    return "DResultatsCurvi()";
  }

  // COR
  /**
   * Lit le fichier de nom <code>_fichier</code> et d'extension ".cor" et renvoie la structure
   * <code>SResultatsCurviCOR</code> obtenue.
   *
   * @param _fichier le nom du fichier a lire sans extension.
   * @return La structure lue a partir de <code>_fichier</code>.
   */
  static SResultatsCurviCOR litResultatsCurviCOR(final String _fichier) {
    try {
      int nsol, i, j;
      int[] fmt;
      FortranReader fcor = new FortranReader(new FileReader(_fichier + ".cor"));
      fmt = new int[] { 5, 5, 5, 10, 10, 10 };
      fcor.readFields(fmt);
      nsol = fcor.intField(0);
      System.out.println("COR nombre de solutions: " + nsol);
      final SResultatsCurviCOR r = new SResultatsCurviCOR();
      r.nbNoeuds = nsol;
      r.lignes = new SResultatsLigneCurviCOR[nsol];
      for (i = 0; i < nsol; i++) {
        fmt = new int[] { 5, 10, 10 };
        fcor.readFields(fmt);
        j = fcor.intField(0);
        if (j != i + 1) {
          System.err.println("COR D�calage en ligne : " + (i + 1));
        }
        final double x = fcor.doubleField(1);
        final double y = fcor.doubleField(2);
        // System.out.println("COR "+i+": Point("+x+","+y+")");
        r.lignes[i] = new SResultatsLigneCurviCOR(i, x, y);
      }
      fmt = new int[] { 5 };
      fcor.readFields(fmt);
      j = fcor.intField(0);
      if (j != -1) {
        System.err.println("Fin de fichier attendue (-1)");
      }
      fcor = null;
      return r;
    } catch (final Exception ex) {
      CDodico.exception(ex);
    }
    return null;
  }

  /**
   * Renvoie la structure "COR" (cf curvi.idl).
   */
  public SResultatsCurviCOR resultatsCurviCOR() {
    return resultsCOR_;
  }

  /**
   * Modifie la structure de resultats "COR".
   *
   * @param _r la nouvelle structure.
   */
  public void resultatsCurviCOR(final SResultatsCurviCOR _r) {
    resultsCOR_ = _r;
  }

  /**
   * Renvoie la <code>_n</code>+1 ieme Structure <code>SResultatsLigneCurviCOR</code> correspondante a la structure
   * <code>SResultatsCurviCOR</code> utilisee.
   */
  public SResultatsLigneCurviCOR resultatsLigneCurviCOR(final int _n) {
    SResultatsLigneCurviCOR r = null;
    try {
      r = resultsCOR_.lignes[_n];
    } catch (final Exception ex) {
      CDodico.exception(this, ex);
    }
    return r;
  }

  // ELE
  /**
   * Lit le fichier de nom <code>_fichier</code> et d'extension ".ele" et complete la structure a renvoyer.
   *
   * @param _fichier le nom du fichier a lire sans extension.
   * @return La structure lue a partir de <code>_fichier</code>.
   */
  static SResultatsCurviELE litResultatsCurviELE(final String _fichier) {
    try {
      int nele, i, j;
      int[] fmt;
      FortranReader fele = new FortranReader(new FileReader(_fichier + ".ele"));
      fmt = new int[] { 5, 5, 5, 5, 5 };
      fele.readFields(fmt);
      nele = fele.intField(0);
      System.out.println("ELE nombre d'�l�ments: " + nele);
      final SResultatsCurviELE r = new SResultatsCurviELE();
      r.nbElements = nele;
      r.lignes = new SResultatsLigneCurviELE[nele];
      for (i = 0; i < nele; i++) {
        fmt = new int[] { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 };
        fele.readFields(fmt);
        j = fele.intField(0);
        if (j != i + 1) {
          System.err.println("ELE D�calage en ligne : " + (i + 1));
        }
        final int c1 = fele.intField(1);
        final int c2 = fele.intField(2);
        final int c3 = fele.intField(3);
        final int c4 = fele.intField(4);
        final int c5 = fele.intField(5);
        final int n1 = fele.intField(6);
        final int n2 = fele.intField(7);
        final int n3 = fele.intField(8);
        final int n4 = fele.intField(9);
        final int z = fele.intField(10);
        // System.out.println("ELE "+i);
        r.lignes[i] = new SResultatsLigneCurviELE(i, c1, c2, c3, c4, c5, n1, n2, n3, n4, z);
      }
      fmt = new int[] { 5 };
      fele.readFields(fmt);
      j = fele.intField(0);
      if (j != -1) {
        System.err.println("Fin de fichier attendue (-1)");
      }
      fele = null;
      return r;
    } catch (final Exception ex) {
      CDodico.exception(ex);
    }
    return null;
  }

  /**
   * Renvoie la structure "ele" utilisee.
   */
  public SResultatsCurviELE resultatsCurviELE() {
    return resultsELE_;
  }

  /**
   * Modifie la structure "ele" utilisee.
   *
   * @param _r la nouvelle structure.
   */
  public void resultatsCurviELE(final SResultatsCurviELE _r) {
    resultsELE_ = _r;
  }

  /**
   * Renvoie la <code>_n</code>+1 ieme Structure <code>SResultatsLigneCurviELE</code> correspondante a la structure
   * <code>SResultatsCurviELE</code> utilisee.
   */
  public SResultatsLigneCurviELE resultatsLigneCurviELE(final int _n) {
    SResultatsLigneCurviELE r = null;
    try {
      r = resultsELE_.lignes[_n];
    } catch (final Exception ex) {
      CDodico.exception(this, ex);
    }
    return r;
  }

  // SOL
  /**
   * Lit le fichier de nom <code>_fichier</code> et d'extension ".sol" et complete la structure a renvoyer.
   * <code>_nsol</code> correspond au nombre de structure <code>SResultatsLigneCurviSOL</code> a lire.
   *
   * @param _fichier le nom du fichier a lire sans extension.
   * @param _nsol le nombre de structure <code>SResultatsLigneCurviSOL</code> a lire.
   * @return La structure lue a partir de <code>_fichier</code>.
   */
  static SResultatsCurviSOL litResultatsCurviSOL(final String _fichier, final int _nsol) {
    try {
      int i, j;
      int[] fmt;
      System.out.println("SOL nombre de solutions: " + _nsol);
      FortranReader fsol = new FortranReader(new FileReader(_fichier + ".sol"));
      final SResultatsCurviSOL r = new SResultatsCurviSOL();
      r.lignes = new SResultatsLigneCurviSOL[_nsol];
      fmt = new int[] { 5 };
      fsol.readFields(fmt);
      j = fsol.intField(0);
      if (j != -999) {
        System.err.println("SOL D�but de fichier attendu (-999)");
      }
      fmt = new int[] { 6, 13, 5, 19, 5 };
      fsol.readFields(fmt);
      j = fsol.intField(2);
      System.out.println("SOL Pas       : " + j);
      j = fsol.intField(4);
      System.out.println("SOL Iteration : " + j);
      fmt = new int[] { 5, 5 };
      fsol.readFields(fmt);
      j = fsol.intField(0);
      if (j != -1) {
        System.out.println("SOL -1 attendu et non " + j);
      }
      j = fsol.intField(1);
      System.out.println("SOL Colonnes  : " + j);
      final int ncol = j;
      fmt = new int[ncol + 1];
      fmt[0] = 5;
      for (j = 1; j < fmt.length; j++) {
        fmt[j] = 12;
      }
      for (i = 0; i < _nsol; i++) {
        fsol.readFields(fmt);
        j = fsol.intField(0);
        if (j != (i + 1)) {
          System.err.println("SOL D�calage en ligne : " + (i + 1));
        }
        // System.out.print("SOL "+i+": ");
        r.lignes[i] = new SResultatsLigneCurviSOL(i, new double[ncol]);
        for (j = 0; j < ncol; j++) {
          r.lignes[i].v[j] = fsol.doubleField(j + 1);
          // System.out.print(" "+r.lignes[i].v[j]);
        }
        // System.out.println("");
      }
      fmt = new int[] { 5 };
      fsol.readFields(fmt);
      j = fsol.intField(0);
      if (j != -1) {
        System.err.println("Fin de fichier attendue (-1)");
      }
      fsol = null;
      return r;
    } catch (final Exception ex) {
      CDodico.exception(ex);
    }
    return null;
  }

  /**
   * Renvoie la structure <code>SResultatsCurviSOL</code> utilisee.
   */
  public SResultatsCurviSOL resultatsCurviSOL() {
    return resultsSOL_;
  }

  /**
   * Modifie la structure <code>SResultatsCurviSOL</code> utilisee.
   *
   * @param _r la nouvelle structure.
   */
  public void resultatsCurviSOL(final SResultatsCurviSOL _r) {
    resultsSOL_ = _r;
  }

  /**
   * Renvoie la <code>_n</code>+1 ieme Structure <code>SResultatsLigneCurviSOL</code> correspondante a la structure
   * <code>SResultatsCurviSOL</code> utilisee.
   *
   * @param _n la ligne demandee
   */
  public SResultatsLigneCurviSOL resultatsLigneCurviSOL(final int _n) {
    SResultatsLigneCurviSOL r = null;
    try {
      r = resultsSOL_.lignes[_n];
    } catch (final Exception ex) {
      CDodico.exception(this, ex);
    }
    return r;
  }
  // Maillage
  /*
   * public static IMaillage construitMaillage (SResultatsCurviCOR _rcor,SResultatsCurviELE _rele) { IMaillage r=null;
   * try { int i; INoeud[] noeuds=new INoeud[_rcor.nbNoeuds]; for(i=0;i <_rcor.nbNoeuds;i++) noeuds[i]=new CNoeud( new
   * CPoint(_rcor.lignes[i].x, _rcor.lignes[i].y,0.)); noeuds[i].numero(_rcor.lignes[i].n); IElement[] elements=new
   * IElement[_rele.nbElements]; for(i=0;i <_rele.nbElements;i++) { INoeud[] nds=new INoeud[4];
   * nds[0]=noeuds[_rele.lignes[i].n1-1]; nds[1]=noeuds[_rele.lignes[i].n2-1]; nds[2]=noeuds[_rele.lignes[i].n3-1];
   * nds[3]=noeuds[_rele.lignes[i].n4-1]; elements[i]=new CElement(nds,LTypeElement.Q4);
   * elements[i].numero(_rele.lignes[i].n); } r=new CMaillage(elements); } catch(Exception ex) {
   * CDodico.exception(CResultatsCurvi.class,ex); } return r; } public IMaillage maillage() { if(maillage_==null)
   * maillage_=construitMaillage(resultatsCurviCOR(),resultatsCurviELE()); return maillage_; } // Carte public static
   * ICarteReel construitCarte (IMaillage _maillage, SResultatsCurviSOL _rsol, LGrandeur _grandeur, int _colonne) {
   * ICarteReel r=null; try { int i; double[] valeurs; int RL=_rsol.lignes.length; valeurs=new double[RL]; for(i=0;i
   * <RL;i++) valeurs[i]=_rsol.lignes[i].v[_colonne]; r=new CCarteReel(_grandeur, _maillage, valeurs); } catch(Exception
   * ex) { CDodico.exception(CResultatsCurvi.class,ex); } return r; } public static ICarteReel construitCarteBathymetrie
   * (IMaillage _maillage, SResultatsCurviSOL _rsol) { return construitCarte(_maillage,_rsol,LGrandeur.PROFONDEUR,2); }
   * public static ICarteReel construitCarteHauteur (IMaillage _maillage, SResultatsCurviSOL _rsol) { return
   * construitCarte(_maillage,_rsol,LGrandeur.HAUTEUR_HOULE,1); } public static ICarteReel construitCartePhase
   * (IMaillage _maillage, SResultatsCurviSOL _rsol) { return construitCarte(_maillage,_rsol,LGrandeur.PHASE,0); }
   * public static ICarteReel construitCarteDeferlement (IMaillage _maillage, ICarteReel _hauteur, ICarteReel
   * _bathymetrie, double _critere) { ICarteReel r=null; try { int i; double[] valeurs; double[]
   * hval=_hauteur.valeurs(); double[] bval=_bathymetrie.valeurs(); int RL=hval.length; valeurs=new double[RL];
   * for(i=0;i <RL;i++) valeurs[i]=(hval[i]>0.78*bval[i]) ? 1. : 0.; r=new CCarteReel(LGrandeur.COEFFICIENT, _maillage,
   * valeurs); } catch(Exception ex) { CDodico.exception(CResultatsCurvi.class,ex); } return r; } // Carte private
   * ICarteReel carte(LGrandeur _grandeur, int _colonne) { return construitCarte(maillage(),resultatsCurviSOL(),
   * _grandeur,_colonne); } public ICarteReel carteBathymetrie() { if(carteBathymetrie_==null)
   * carteBathymetrie_=carte(LGrandeur.PROFONDEUR,2); return carteBathymetrie_; } public ICarteReel carteHauteur() {
   * if(carteHauteur_==null) carteHauteur_=carte(LGrandeur.HAUTEUR_HOULE,1); return carteHauteur_; } public ICarteReel
   * cartePhase() { if(cartePhase_==null) cartePhase_=carte(LGrandeur.PHASE,0); return cartePhase_; } public ICarteReel
   * carteDeferlement(double _critere) { if(carteDeferlement_==null) carteDeferlement_=construitCarteDeferlement
   * (maillage(),carteHauteur(),carteBathymetrie(),_critere); return carteDeferlement_; }
   */
}