/*
 * @creation     2000-12-05
 * @modification $Date: 2006-09-19 14:42:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.curvi;
import org.fudaa.dodico.corba.calcul.IResultats;
import org.fudaa.dodico.corba.calcul.IParametres;
import org.fudaa.dodico.corba.curvi.ICalculCurvi;
import org.fudaa.dodico.corba.curvi.ICalculCurviHelper;
import org.fudaa.dodico.corba.curvi.ICalculCurviOperations;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.objet.IObjet;
import org.fudaa.dodico.corba.objet.IPersonne;
import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.UsineLib;
import org.fudaa.dodico.calcul.DCalcul;
import java.util.Date;
/**
 * Un pont vers CalculCurvi. Une instance de ICalculCurvi est referencee en
 * interne et une partie des operations sont deleguees a cet objet.
 *
 * @version      $Revision: 1.10 $ $Date: 2006-09-19 14:42:27 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class PontCurvi extends DCalcul implements ICalculCurviOperations,ICalculCurvi {
  /**
   * Le calcul <code>Curvi</code> vers lequel le "pont" est effectue.
   */
  ICalculCurvi curvi_;
  /**
   * Constructeur qui recherche un serveur Curvi (<code>ICalculCurvi</code>).
   */
  public PontCurvi() {
    super();
    /*
    curvi_=ICalculCurviHelper.narrow
      (CDodico.findServerByInterface("::curvi::ICalculCurvi"));
      */
    curvi_=
      ICalculCurviHelper.narrow(
        CDodico.findServerByName("ICalculCurvi-xeena-78711030"));
    System.err.println("server: " + curvi_);
  }
  /**
   * Pas implanter completement.
   *
   * @return     <code>new PontCurvi()</code>
   */
  public final Object clone()  throws CloneNotSupportedException{
    return new PontCurvi();
  }
  /**
   * Description de l'objet.
   *
   * @return     "PontCurvi()".
   */
  public String toString() {
    return "PontCurvi()";
  }
  // IObjet
  /**
   * Appelle "l'initialisateur" du ICalculCurvi (soit DCalculCurvi) interne.
   *
   * @see        org.fudaa.dodico.curvi.DCalculCurvi
   */
  public void initialise(final IObjet _o) {
    curvi_.initialise(_o);
  }
  /**
   * Methode deleguee au ICalculCurvi interne.
   *
   * @return     moduleCorba de ICalculCurvi.
   * @see        org.fudaa.dodico.curvi.DCalculCurvi
   */
  final public String moduleCorba() {
    return curvi_.moduleCorba();
  }
  /**
   * Les interfaces corba du ICalculCurvi interne.
   *
   * @see        org.fudaa.dodico.curvi.DCalculCurvi
   */
  final public String[] interfacesCorba() {
    return curvi_.interfacesCorba();
  }
  // ICalcul
  /**
   * Instant en secondes correspondant a la creation du ICalculCurvi interne.
   */
  public int creation() {
    return curvi_.creation();
  }
  /**
   * Instant en secondes correspondant a la derniere utilisation du ICalculCurvi
   * interne.
   */
  public int derniereUtilisation() {
    return curvi_.derniereUtilisation();
  }
  /**
   * Responsable du ICalculCurvi.
   */
  public IPersonne responsable() {
    return curvi_.responsable();
  }
  /**
   * Duree estimee du calcul.
   */
  public int dureeEstimee(final IConnexion _c) {
    return curvi_.dureeEstimee(_c);
  }
  /**
   * Description du pont vers le ICalculCurvi.
   */
  public String description() {
    return "Curvi, pont: " + super.description();
  }
  // Curvi
  /**
   * Les IParametres du ICalculCurvi.
   */
  public IParametres parametres(final IConnexion _c) {
    return curvi_.parametres(_c);
  }
  /**
   * Les IResultats du ICalculCurvi.
   */
  public IResultats resultats(final IConnexion _c) {
    return curvi_.resultats(_c);
  }
  /**
   * Lancement du calcul.
   *
   */
  public void calcul(final IConnexion _c) {
    curvi_.calcul(_c);
  }
  /**
   * Creation et connexion a l'orb d'une instance de <code>DPontCurvi</code>.
   * Si non vide, le premier argument passe est utilise comme nom de connexion.
   * Sinon un nom est genere par <code>CDodico.generateName(String)</code>.
   *
   * @param      _args le premier argument sert de nom de connexion.
   * @see        org.fudaa.dodico.objet.CDodico#generateName(String)
   */
  public static void main(final String[] _args) {
    final String nom=
      (_args.length > 0 ? _args[0] : CDodico.generateName("::curvi::PontCurvi"));
    CDodico.rebind(nom, UsineLib.createService(PontCurvi.class));
    System.out.println("Curvi bridge running... ");
    System.out.println("Name: " + nom);
    System.out.println("Date: " + new Date());
  }
}
