/*
 * @creation     1997-12-10
 * @modification $Date: 2006-09-19 14:42:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.curvi;
import org.fudaa.dodico.corba.curvi.ICalculCurvi;
import org.fudaa.dodico.corba.curvi.ICalculCurviOperations;
import org.fudaa.dodico.corba.curvi.IParametresCurvi;
import org.fudaa.dodico.corba.curvi.IResultatsCurvi;
import org.fudaa.dodico.corba.curvi.IParametresCurviHelper;
import org.fudaa.dodico.corba.curvi.IResultatsCurviHelper;
import org.fudaa.dodico.corba.curvi.SParametresCurviINP;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.calcul.DCalcul;
import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.CExec;
import java.io.File;
import java.io.FileReader;
import java.io.LineNumberReader;
/**
 * Gestion du lancement du code de calcul <code>Curvi</code>.
 *
 * @version      $Id: DCalculCurvi.java,v 1.10 2006-09-19 14:42:27 deniger Exp $
 * @author       Guillaume Desnoix
 */
public class DCalculCurvi extends DCalcul implements ICalculCurviOperations,ICalculCurvi {
  /**
   * Initialisation des extensions des fichiers (asc, cor, ele, sol, ini, fin
   * , out).
   */
  public DCalculCurvi() {
    super();
    setFichiersExtensions(
      new String[] { ".asc", ".cor", ".ele", ".sol", ".ini", ".fin", getOutExt() });
  }
  private static String getOutExt() {
    return ".out";
  }
  /**
   * Pas implenter completement.
   *
   * @return     <code>new DCalculCurvi()</code>.
   */
  public final Object clone()  throws CloneNotSupportedException{
    return new DCalculCurvi();
  }
  /**
   * Chaine descriptive de la classe.
   *
   * @return <code>"DCalculCurvi()"</code>.
   */
  public String toString() {
    return "DCalculCurvi()";
  }
  /**
   * Estime le temps necessaire au calcul.
   *
   * @param      _c  la connexion concernee.
   */
  public int dureeEstimee(final IConnexion _c) {
    int t= 0;
    final IParametresCurvi params= IParametresCurviHelper.narrow(parametres(_c));
    if (params != null) {
      final SParametresCurviINP p= params.parametresINP();
      if (p.test == 1) {
        t= 3;
      } else if (p.test == 2) {
        t= 12;
      } else if (p.test == 3) {
        t= 11;
      } else {
        t= 20;
      }
      t *= p.nbMaillesXI * p.nbMaillesETA / 10;
    }
    return t + 5;
  }
  /**
   * Description du serveur de calcul <code>Curvi</code>.
   */
  public String description() {
    return "Curvi, serveur de calcul pour la houle: " + super.description();
  }
  /**
   * Lance le code de calcul Curvi : verifie la non-nullite des interfaces
   * parametres et resultats,lance l'executable et finalement lit les fichiers
   * de resultats pour completer l'interface IResultats.
   *
   * @param      _c  la connexion utilisee.
   */
  public void calcul(final IConnexion _c) {
    //verifie la connexion et les interfaces des parametres et des resultats.
    if (!verifieConnexion(_c)) {
      return;
    }
    final IParametresCurvi params= IParametresCurviHelper.narrow(parametres(_c));
    if (params == null) {
      CDodico.exceptionAxel(this, new Exception("params non definis (null)"));
      return;
    }
    final IResultatsCurvi results= IResultatsCurviHelper.narrow(resultats(_c));
    if (results == null) {
      CDodico.exceptionAxel(this, new Exception("results non definis (null)"));
      return;
    }
    log(_c, "lancement du calcul");
    //les parametres neccessaires au lancement du code.
    final String os= System.getProperty("os.name");
    final String path= cheminServeur();
    final String fichier= path + "curvi" + _c.numero();
    try {
      DParametresCurvi.ecritParametresCurviINP(fichier, params.parametresINP());
      DParametresCurvi.ecritParametresCurviASC(fichier, params.parametresASC());
      //construction de la commande a lancer
      System.out.println("Appel de l'ex�cutable curvi pour " + fichier);
      String[] cmd;
      if (os.startsWith("Windows")) {
        cmd= new String[2];
        cmd[0]= path + "curvi.bat";
        cmd[1]= fichier;
        System.out.println(cmd[0] + " " + cmd[1]);
      } else {
        cmd= new String[2];
        cmd[0]= path + "curvi.sh";
        cmd[1]= fichier;
        System.out.println(cmd[0] + " " + cmd[1]);
      }
      //lancement de l'executable
      final CExec ex= new CExec();
      ex.setCommand(cmd);
      ex.setOutStream(System.out);
      ex.setErrStream(System.err);
      ex.exec();
      System.out.println("Fin du calcul");
      System.out.println("R�sultats de " + fichier + getOutExt());
      if (new File(fichier + getOutExt()).exists()) {
        final LineNumberReader fr=
          new LineNumberReader(new FileReader(fichier + getOutExt()));
        while (fr.ready()) {
          final String s= fr.readLine();
          if (!s.equals("")) {
            System.out.println(s);
          }
        }
      }
      //lecture des fichiers resultats.
      results.resultatsCurviCOR(DResultatsCurvi.litResultatsCurviCOR(fichier));
      results.resultatsCurviELE(DResultatsCurvi.litResultatsCurviELE(fichier));
      final int nsol= results.resultatsCurviCOR().nbNoeuds;
      results.resultatsCurviSOL(
        DResultatsCurvi.litResultatsCurviSOL(fichier, nsol));
      log(_c, "calcul termin�");
    } catch (final Exception ex) {
      log(_c, "erreur du calcul");
      CDodico.exceptionAxel(this, ex);
    }
  }
}
