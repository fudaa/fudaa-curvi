/*
 * @creation     1998-03-31
 * @modification $Date: 2006-09-19 14:42:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.curvi;

import org.fudaa.dodico.corba.curvi.IParametresCurvi;
import org.fudaa.dodico.corba.curvi.IParametresCurviOperations;
import org.fudaa.dodico.corba.curvi.SParametresCurviASC;
import org.fudaa.dodico.corba.curvi.SParametresCurviINP;
import org.fudaa.dodico.calcul.DParametres;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.objet.CDodico;
import java.io.FileWriter;

/**
 * Les parametres du code Curvi.
 *
 * @version $Revision: 1.9 $ $Date: 2006-09-19 14:42:27 $ by $Author: deniger $
 * @author Guillaume Desnoix
 */
public class DParametresCurvi extends DParametres implements IParametresCurviOperations, IParametresCurvi {
  /**
   * Les parametres INP (voir le fichier curvi.idl).
   */
  private SParametresCurviINP paramsINP_;
  /**
   * Les parametres ASC (voir le fichier curvi.idl).
   */
  private SParametresCurviASC paramsASC_;

  public DParametresCurvi() {
    super();
  }

  /**
   * Pas implanter completement.
   *
   * @return <code>new DParametresCurvi()</code>.
   */
  public final Object clone() throws CloneNotSupportedException {
    return new DParametresCurvi();
  }

  /**
   * Description de la classe.
   *
   * @return <code>"DParametresCurvi()"</code>.
   */
  public String toString() {
    return "DParametresCurvi()";
  }

  // INP
  /**
   * Renvoie les parametres INP utilises.(voir le fichier curvi.idl).
   */
  public SParametresCurviINP parametresINP() {
    return paramsINP_;
  }

  /**
   * Modifications des parametres INP.
   */
  public void parametresINP(final SParametresCurviINP _p) {
    paramsINP_ = _p;
  }

  /**
   * Ecrit les parametres INP dans le fichier <code>_fichier.inp</code> dans le "format" Fortran.
   *
   * @param _fichier le nom du fichier destinaire (sans extension).
   * @param _parametres les parametres ecrits dans le fichier.
   * @see org.fudaa.dodico.fortran.FortranWriter
   */
  public static void ecritParametresCurviINP(final String _fichier, final SParametresCurviINP _parametres) {
    System.out.println("Ecriture de " + _fichier + ".inp");
    try {
      final FortranWriter finp = new FortranWriter(new FileWriter(_fichier + ".inp"));
      // System.err.println("FINP="+finp);
      int[] fmt;
      fmt = new int[] { 12 };
      finp.stringField(0, _fichier + ".asc");
      finp.writeFields(fmt);
      finp.stringField(0, "");
      finp.writeFields(fmt);
      finp.stringField(0, "");
      finp.writeFields(fmt);
      finp.stringField(0, _fichier + ".cor");
      finp.writeFields(fmt);
      finp.stringField(0, _fichier + ".ele");
      finp.writeFields(fmt);
      finp.stringField(0, _fichier + ".sol");
      finp.writeFields(fmt);
      finp.stringField(0, "");
      finp.writeFields(fmt);
      finp.stringField(0, "");
      finp.writeFields(fmt);
      finp.stringField(0, _fichier + ".ini");
      finp.writeFields(fmt);
      finp.stringField(0, _fichier + ".fin");
      finp.writeFields(fmt);
      fmt = new int[] { 10 };
      finp.intField(0, 1);
      finp.writeFields(fmt);
      finp.doubleField(0, _parametres.periodeHoule);
      finp.writeFields(fmt);
      finp.doubleField(0, _parametres.hauteurHoule);
      finp.writeFields(fmt);
      finp.doubleField(0, _parametres.hauteurMer);
      finp.writeFields(fmt);
      finp.doubleField(0, _parametres.incidenceHoule);
      finp.writeFields(fmt);
      finp.intField(0, _parametres.nbMaillesXI);
      finp.writeFields(fmt);
      finp.intField(0, _parametres.nbMaillesETA);
      finp.writeFields(fmt);
      finp.doubleField(0, _parametres.xd1);
      finp.writeFields(fmt);
      finp.doubleField(0, _parametres.yd1);
      finp.writeFields(fmt);
      finp.doubleField(0, _parametres.xd2);
      finp.writeFields(fmt);
      finp.doubleField(0, _parametres.yd2);
      finp.writeFields(fmt);
      finp.doubleField(0, _parametres.xd3);
      finp.writeFields(fmt);
      finp.doubleField(0, _parametres.yd3);
      finp.writeFields(fmt);
      finp.intField(0, _parametres.lissage);
      finp.writeFields(fmt);
      finp.intField(0, _parametres.test);
      finp.writeFields(fmt);
      finp.intField(0, _parametres.repriseVag);
      finp.writeFields(fmt);
      finp.flush();
      finp.close();
    } catch (final Exception ex) {
      CDodico.exception(DParametresCurvi.class, ex);
    }
  }

  // ASC
  /**
   * Renvoie les parametres ASC utilises.(voir le fichier curvi.idl).
   */
  public SParametresCurviASC parametresASC() {
    return paramsASC_;
  }

  /**
   * Modification des parametres ASC.
   */
  public void parametresASC(final SParametresCurviASC _p) {
    paramsASC_ = _p;
  }

  /**
   * Ecrit les parametres <code>_parametres</code> dans le fichier de destination <code>_fichier.asc</code> dans le
   * "format" Fortran.
   *
   * @param _fichier le nom du fichier de destination sans extension.
   * @param _parametres les parametres a ecrire.
   */
  public static void ecritParametresCurviASC(final String _fichier, final SParametresCurviASC _parametres) {
    if (_parametres == null) {
      System.out.println("Param�tres ASC internes � Curvi");
      return;
    }
    System.out.println("Ecriture de " + _fichier + ".asc");
    try {
      final FortranWriter fasc = new FortranWriter(new FileWriter(_fichier + ".asc"));
      int[] fmt;
      fmt = new int[] { 5 };
      fasc.intField(0, _parametres.nbMaillesXI);
      fasc.writeFields(fmt);
      fasc.intField(0, _parametres.nbMaillesETA);
      fasc.writeFields(fmt);
      fmt = new int[] { 12, 12, 12 };
      for (int i = 0; i < _parametres.lignes.length; i++) {
        fasc.doubleField(0, _parametres.lignes[i].x);
        fasc.doubleField(1, _parametres.lignes[i].y);
        fasc.doubleField(2, _parametres.lignes[i].z);
        fasc.writeFields(fmt);
      }
      fasc.flush();
      fasc.close();
    } catch (final Exception ex) {
      CDodico.exception(DParametresCurvi.class, ex);
    }
  }
}
