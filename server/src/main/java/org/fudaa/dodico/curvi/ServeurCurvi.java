/*
 * @creation     2000-02-16
 * @modification $Date: 2006-09-19 14:42:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.curvi;
import java.util.Date;

import org.fudaa.dodico.objet.UsineLib;
import org.fudaa.dodico.objet.CDodico;
/**
 * Lance le serveur du code de calcul <code>curvi</code>.
 *
 * @version      $Revision: 1.12 $ $Date: 2006-09-19 14:42:27 $ by $Author: deniger $
 * @author       Guillaume Desnoix 
 */
public final class ServeurCurvi {
  
  private ServeurCurvi(){
    
  }
  /**
   * Creation et connexion a l'orb d'une instance de <code>DCalculCurvi</code>.
   * Si non vide, le premier argument passe est utilise comme nom de connexion.
   * Sinon un nom est genere par <code>CDodico.generateName(String)</code>. 
   *
   * @param      _args le premier argument sert de nom de connexion.
   * @see        org.fudaa.dodico.objet.CDodico#generateName(String)
   */
  public static void main(final String[] _args) {
    final String nom=
      (_args.length > 0
        ? _args[0]
        : CDodico.generateName("::curvi::ICalculCurvi"));
    //Cas particulier : il s'agit de creer un serveur de calcul dans une jvm donne
    //Cette M�thode n'est pas a imiter. If faut utiliser Boony pour creer des objet corba.
    CDodico.rebind(nom, UsineLib.createService(DCalculCurvi.class));
    System.out.println("Curvi server running... ");
    System.out.println("Name: " + nom);
    System.out.println("Date: " + new Date());
  }
}
