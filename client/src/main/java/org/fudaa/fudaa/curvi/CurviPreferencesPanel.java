/*
 * @file         CurviPreferencesPanel.java
 * @creation     1999-01-13
 * @modification $Date: 2006-09-19 15:02:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.curvi;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuVerticalLayout;
/**
 * Panneau de preferences pour Curvi.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:02:25 $ by $Author: deniger $
 * @author       Guillaume Desnoix 
 */
public class CurviPreferencesPanel
  extends BuAbstractPreferencesPanel
  implements ActionListener {
  CurviPreferences options_;
  CurviImplementation curvi_;
  JCheckBox cbDifferentesPalettes;
  JCheckBox cbDialoguesCalcul;
  public String getTitle() {
    return "Curvi";
  }
  // Constructeur
  public CurviPreferencesPanel(final CurviImplementation _curvi) {
    super();
    options_= CurviPreferences.CURVI;
    curvi_= _curvi;
    final BuVerticalLayout lo= new BuVerticalLayout();
    lo.setVgap(5);
    final JPanel p= new JPanel();
    p.setBorder(
      new CompoundBorder(
        new TitledBorder(getTitle()),
        new EmptyBorder(5, 5, 5, 5)));
    p.setLayout(lo);
    cbDifferentesPalettes=
      new JCheckBox(CurviResource.CURVI.getString("Différentes palettes"));
    cbDialoguesCalcul=
      new JCheckBox(CurviResource.CURVI.getString("Dialogues de calcul"));
    cbDifferentesPalettes.setName("cbPALETTES_DIFFERENTES");
    cbDialoguesCalcul.setName("cbDIALOGUES_CALCUL");
    p.add(cbDifferentesPalettes);
    p.add(cbDialoguesCalcul);
    p.add(new JPanel());
    setLayout(new BuBorderLayout());
    setBorder(new EmptyBorder(5, 5, 5, 5));
    add(p, BuBorderLayout.CENTER);
    updateComponents();
  }
  // Evenements
  public void actionPerformed(final ActionEvent _evt) {
    final JComponent source= (JComponent)_evt.getSource();
    System.err.println("SOURCE=" + source.getName());
    System.err.println("ACTION=" + _evt.getActionCommand());
    // chBrowser.setEnabled(cbExterne.isSelected());
  }
  // Methodes publiques
  public boolean isPreferencesValidable() {
    return true;
  }
  public void validatePreferences() {
    fillTable();
    options_.writeIniFile();
  }
  public boolean isPreferencesApplyable() {
    return false;
  }
  public void applyPreferences() {
    fillTable();
    options_.applyOn(curvi_);
  }
  public boolean isPreferencesCancelable() {
    return true;
  }
  public void cancelPreferences() {
    options_.readIniFile();
    updateComponents();
  }
  // Methodes privees
  private void fillTable() {
    if (cbDifferentesPalettes.isSelected()) {
      options_.putBooleanProperty("palettes.differentes", true);
    }
    if (cbDialoguesCalcul.isSelected()) {
      options_.putBooleanProperty("calcul.dialogues", true);
    }
  }
  private void updateComponents() {
    cbDifferentesPalettes.setSelected(
      options_.getBooleanProperty("palettes.differentes"));
    cbDialoguesCalcul.setSelected(
      options_.getBooleanProperty("calcul.dialogues"));
  }
}
