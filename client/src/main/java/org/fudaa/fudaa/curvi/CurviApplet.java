/*
 * @file         CurviApplet.java
 * @creation     1999-01-13
 * @modification $Date: 2007-05-04 13:59:04 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.curvi;
import com.memoire.bu.BuApplet;
/**
 * @version      $Revision: 1.6 $ $Date: 2007-05-04 13:59:04 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class CurviApplet extends BuApplet {
  public CurviApplet() {
    super();
    System.out.println("Client Curvi");
    /*
    CurviImplementation.SERVEUR_CURVI=
      ICalculCurviHelper.narrow
        (CDodico.findServer
    ("::curvi::ICalculCurvi","un-serveur-curvi",15000));
    */
    /*
    do
    {
      try
      {
    CurviImplementation.SERVEUR_CURVI=
    ICalculCurviHelper.narrow(CDodico.findServerByName("un-serveur-curvi"));
      }
      catch(Exception ex)
      {
    CurviImplementation.SERVEUR_CURVI=null;
    System.err.println(""+ex);
      }
    }
    while(CurviImplementation.SERVEUR_CURVI==null);
    */
    setImplementation(new CurviImplementation());
  }
  public void destroy() {
    CurviImplementation.SERVEUR_CURVI= null;
    //System.gc();
  }
}
