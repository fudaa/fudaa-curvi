/*
 * @file         CurviTableauResultats.java
 * @creation     1998-09-16
 * @modification $Date: 2006-09-19 15:02:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.curvi;
import java.text.NumberFormat;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import com.memoire.bu.BuTable;
import com.memoire.bu.BuTableCellRenderer;

import org.fudaa.dodico.corba.curvi.SResultatsCurviCOR;
import org.fudaa.dodico.corba.curvi.SResultatsCurviSOL;
/**
 * Un tableau des résultats.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:02:25 $ by $Author: deniger $
 * @author       Guillaume Desnoix 
 */
public class CurviTableauResultats extends BuTable {
  public CurviTableauResultats(
    final SResultatsCurviCOR _rcor,
    final SResultatsCurviSOL _rsol) {
    super(new CurviTableauResultatsModel(_rcor, _rsol));
    setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    final TableCellRenderer tcr= new BuTableCellRenderer();
    for (int i= 0; i < getModel().getColumnCount(); i++) {
      getColumn(getColumnName(i)).setCellRenderer(tcr);
    }
    final BuTableCellRenderer tcrI= new BuTableCellRenderer();
    final BuTableCellRenderer tcrXY= new BuTableCellRenderer();
    final BuTableCellRenderer tcrZ= new BuTableCellRenderer();
    final NumberFormat fmtXY= NumberFormat.getInstance();
    final NumberFormat fmtZ= NumberFormat.getInstance();
    fmtXY.setMinimumFractionDigits(2);
    fmtXY.setMaximumFractionDigits(2);
    fmtZ.setMinimumFractionDigits(3);
    fmtZ.setMaximumFractionDigits(3);
    tcrXY.setNumberFormat(fmtXY);
    tcrZ.setNumberFormat(fmtZ);
    getColumn(getColumnName(0)).setCellRenderer(tcrI);
    getColumn(getColumnName(1)).setCellRenderer(tcrXY);
    getColumn(getColumnName(2)).setCellRenderer(tcrXY);
    getColumn(getColumnName(3)).setCellRenderer(tcrZ);
    getColumn(getColumnName(4)).setCellRenderer(tcrZ);
    getColumn(getColumnName(5)).setCellRenderer(tcrZ);
    getColumn(getColumnName(0)).setWidth(60);
    getColumn(getColumnName(1)).setWidth(60);
    getColumn(getColumnName(2)).setWidth(60);
    getColumn(getColumnName(3)).setWidth(60);
    getColumn(getColumnName(4)).setWidth(60);
    getColumn(getColumnName(5)).setWidth(60);
  }
}
class CurviTableauResultatsModel implements TableModel {
  Vector listeners_;
  SResultatsCurviCOR rcor_;
  SResultatsCurviSOL rsol_;
  public CurviTableauResultatsModel(
    final SResultatsCurviCOR _rcor,
    final SResultatsCurviSOL _rsol) {
    listeners_= new Vector();
    rcor_= _rcor;
    rsol_= _rsol;
  }
  public Class getColumnClass(final int column) {
    return (column == 0 ? Integer.class : Double.class);
  }
  public int getColumnCount() {
    return 6;
  }
  public String getColumnName(final int column) {
    String r= "";
    switch (column) {
      case 0 :
        r= "I";
        break;
      case 1 :
        r= "X";
        break;
      case 2 :
        r= "Y";
        break;
      case 3 :
        r= "Phase";
        break;
      case 4 :
        r= "Hauteur";
        break;
      case 5 :
        r= "Bathymétrie";
        break;
    }
    return r;
  }
  public int getRowCount() {
    return rcor_.lignes.length;
  }
  public Object getValueAt(final int row, final int column) {
    Object r= null;
    switch (column) {
      case 0 :
        r= new Integer(row);
        break;
      case 1 :
        r= new Double(rcor_.lignes[row].x);
        break;
      case 2 :
        r= new Double(rcor_.lignes[row].y);
        break;
      case 3 :
        r= new Double(rsol_.lignes[row].v[0]);
        break;
      case 4 :
        r= new Double(rsol_.lignes[row].v[1]);
        break;
      case 5 :
        r= new Double(rsol_.lignes[row].v[2]);
        break;
    }
    return r;
  }
  public boolean isCellEditable(final int row, final int column) {
    return false;
  }
  public void setValueAt(final Object value, final int row, final int column) {}
  public void addTableModelListener(final TableModelListener _l) {
    listeners_.addElement(_l);
  }
  public void removeTableModelListener(final TableModelListener _l) {
    listeners_.removeElement(_l);
  }
}
