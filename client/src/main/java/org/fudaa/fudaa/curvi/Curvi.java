/*
 * @file         Curvi.java
 * @creation     1999-01-13
 * @modification $Date: 2007-01-19 13:14:12 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.curvi;
import org.fudaa.fudaa.commun.impl.Fudaa;
/**
 * @version      $Revision: 1.10 $ $Date: 2007-01-19 13:14:12 $ by $Author: deniger $
 * @author       Guillaume Desnoix 
 */
public class Curvi {
  /**
   * @param args arg de l'appli
   */
  public static void main(final String args[]) {
  final Fudaa f=new Fudaa();
  f.launch(args,CurviImplementation.informationsSoftware(),false);
  f.startApp(new CurviImplementation());
  }
}
