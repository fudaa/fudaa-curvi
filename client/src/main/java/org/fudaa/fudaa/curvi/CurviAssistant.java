/*
 * @file         CurviAssistant.java
 * @creation     1999-01-13
 * @modification $Date: 2005-08-16 13:27:39 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.curvi;
import com.memoire.bu.BuAssistant;
/**
 * @version      $Revision: 1.5 $ $Date: 2005-08-16 13:27:39 $ by $Author: deniger $
 * @author        
 */
public class CurviAssistant extends BuAssistant {}
