/*
 * @file         CurviResource.java
 * @creation     1999-01-13
 * @modification $Date: 2006-09-19 15:02:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.curvi;
import com.memoire.bu.BuResource;

import org.fudaa.fudaa.ressource.FudaaResource;
/**
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:02:25 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class CurviResource extends BuResource {
  public final static CurviResource CURVI=
    new CurviResource(FudaaResource.FUDAA);
  public CurviResource(final BuResource _parent) {
    setParent(_parent);
  }
}
