/*
 * @file         CurviTacheCalcul.java
 * @creation     1999-01-13
 * @modification $Date: 2006-09-19 15:02:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.curvi;
import com.memoire.bu.BuTask;
/**
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:02:26 $ by $Author: deniger $
 * @author       Guillaume Desnoix 
 */
public class CurviTacheCalcul extends BuTask {
  private CurviImplementation app_;
  public CurviTacheCalcul(final CurviImplementation _app) {
    super();
    app_= _app;
    setName("Calcul");
  }
  public void start() {
    super.start();
  }
  public void run() {
    app_.oprCalculer();
    super.run();
  }
}
