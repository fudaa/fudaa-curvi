/*
 * @file         CurviTacheOperation.java
 * @creation     1999-01-13
 * @modification $Date: 2006-09-19 15:02:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.curvi;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.memoire.bu.BuTask;
/**
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:02:25 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class CurviTacheOperation extends BuTask {
  private CurviImplementation app_;
  private String methode_;
  public CurviTacheOperation(
    final CurviImplementation _app,
    final String _nom,
    final String _methode) {
    super();
    app_= _app;
    methode_= _methode;
    setName(_nom);
    setTaskView(app_.getMainPanel().getTaskView());
    setPriority(Thread.MIN_PRIORITY);
  }
  public void start() {
    super.start();
  }
  public void run() {
    try {
      // app_.oprCalculer();
      final Method m= app_.getClass().getMethod(methode_, new Class[0]);
      m.invoke(app_, new Object[0]);
      super.run();
    } catch (final InvocationTargetException ex) {
      System.err.println("CurviTacheOperation: " + ex);
      ex.getTargetException().printStackTrace();
      //suspend();
    } catch (final Exception ex) {
      System.err.println("CurviTacheOperation: " + ex);
      ex.printStackTrace();
      //suspend();
    }
  }
}
