/*
 * @file         CurviTableauASC.java
 * @creation     1998-12-29
 * @modification $Date: 2006-09-19 15:02:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.curvi;
import java.text.NumberFormat;
import java.util.Vector;

import javax.swing.JTable;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import com.memoire.bu.BuTable;
import com.memoire.bu.BuTableCellRenderer;

import org.fudaa.dodico.corba.curvi.SParametresCurviASC;
/**
 * Un tableau du releve bathymetrique.
 *
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:02:25 $ by $Author: deniger $
 * @author       Guillaume Desnoix 
 */
public class CurviTableauASC extends BuTable {
  public CurviTableauASC(final SParametresCurviASC _pasc) {
    super(new CurviTableauASCModel(_pasc));
    setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    final BuTableCellRenderer tcrI= new BuTableCellRenderer();
    final BuTableCellRenderer tcrXY= new BuTableCellRenderer();
    final BuTableCellRenderer tcrZ= new BuTableCellRenderer();
    final NumberFormat fmtXY= NumberFormat.getInstance();
    final NumberFormat fmtZ= NumberFormat.getInstance();
    fmtXY.setMinimumFractionDigits(2);
    fmtXY.setMaximumFractionDigits(2);
    fmtZ.setMinimumFractionDigits(3);
    fmtZ.setMaximumFractionDigits(3);
    tcrXY.setNumberFormat(fmtXY);
    tcrZ.setNumberFormat(fmtZ);
    getColumn(getColumnName(0)).setCellRenderer(tcrI);
    getColumn(getColumnName(1)).setCellRenderer(tcrXY);
    getColumn(getColumnName(2)).setCellRenderer(tcrXY);
    getColumn(getColumnName(3)).setCellRenderer(tcrZ);
    getColumn(getColumnName(0)).setWidth(60);
    getColumn(getColumnName(1)).setWidth(60);
    getColumn(getColumnName(2)).setWidth(60);
    getColumn(getColumnName(3)).setWidth(120);
  }
}
class CurviTableauASCModel implements TableModel {
  Vector listeners_;
  SParametresCurviASC pasc_;
  public CurviTableauASCModel(final SParametresCurviASC _pasc) {
    listeners_= new Vector();
    pasc_= _pasc;
  }
  public Class getColumnClass(final int column) {
    return (column == 0 ? Integer.class : Double.class);
  }
  public int getColumnCount() {
    return 4;
  }
  public String getColumnName(final int column) {
    String r= "";
    switch (column) {
      case 0 :
        r= "I";
        break;
      case 1 :
        r= "X";
        break;
      case 2 :
        r= "Y";
        break;
      case 3 :
        r= "Z";
        break;
    }
    return r;
  }
  public int getRowCount() {
    return pasc_.lignes.length;
  }
  public Object getValueAt(final int row, final int column) {
    Object r= null;
    switch (column) {
      case 0 :
        r= new Integer(row);
        break;
      case 1 :
        r= new Double(pasc_.lignes[row].x);
        break;
      case 2 :
        r= new Double(pasc_.lignes[row].y);
        break;
      case 3 :
        r= new Double(pasc_.lignes[row].z);
        break;
    }
    return r;
  }
  public boolean isCellEditable(final int row, final int column) {
    return (column >= 1);
  }
  public void setValueAt(final Object value, final int row, final int column) {
    switch (column) {
      case 1 :
        pasc_.lignes[row].x= new Double(value.toString()).doubleValue();
        break;
      case 2 :
        pasc_.lignes[row].y= new Double(value.toString()).doubleValue();
        break;
      case 3 :
        pasc_.lignes[row].z= new Double(value.toString()).doubleValue();
        break;
    }
  }
  public void addTableModelListener(final TableModelListener _l) {
    listeners_.addElement(_l);
  }
  public void removeTableModelListener(final TableModelListener _l) {
    listeners_.removeElement(_l);
  }
}
