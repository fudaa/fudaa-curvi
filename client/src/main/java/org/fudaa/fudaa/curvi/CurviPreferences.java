/*
 * @file         CurviPreferences.java
 * @creation     1999-01-13
 * @modification $Date: 2006-09-19 15:02:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.curvi;
import com.memoire.bu.BuPreferences;
/**
 * Preferences pour Curvi.
 *
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:25 $ by $Author: deniger $
 * @author       Guillaume Desnoix 
 */
public class CurviPreferences extends BuPreferences {
  public final static CurviPreferences CURVI= new CurviPreferences();
  public CurviPreferences() {
    super();
    if (getStringProperty("calcul.dialogues") == null) {
      putStringProperty("calcul.dialogues", "true");
    }
  }
  public void applyOn(final Object _o) {
    if (!(_o instanceof CurviImplementation)) {
      throw new RuntimeException("" + _o + " is not a CurviImplementation.");
    //    CurviImplementation _appli=(CurviImplementation)_o;
    }
  }
  public boolean isDifferentesPalettes() {
    return getBooleanProperty("palettes.differentes");
  }
  public boolean isDialoguesCalcul() {
    return getBooleanProperty("calcul.dialogues");
  }
}
