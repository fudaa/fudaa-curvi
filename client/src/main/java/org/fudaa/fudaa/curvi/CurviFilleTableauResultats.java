/*
 * @file         CurviFilleTableauResultats.java
 * @creation     1999-01-13
 * @modification $Date: 2007-01-19 13:14:12 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.curvi;
import org.fudaa.dodico.corba.curvi.SResultatsCurviCOR;
import org.fudaa.dodico.corba.curvi.SResultatsCurviSOL;

import org.fudaa.fudaa.commun.impl.FudaaFilleTableau;
/**
 * @version      $Revision: 1.11 $ $Date: 2007-01-19 13:14:12 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class CurviFilleTableauResultats extends FudaaFilleTableau {
  public CurviFilleTableauResultats(
    final CurviImplementation _app,
    final String _fichier,
    final SResultatsCurviCOR _rcor,
    final SResultatsCurviSOL _rsol) {
    super(_app, _app.getInformationsDocument());
    setName("ifRESULTATS");
    setTitle(_fichier);
    setTable(new CurviTableauResultats(_rcor, _rsol));
  }
}
