/*
 * @file         CurviImplementation.java
 * @creation     1998-10-02
 * @modification $Date: 2007-01-19 13:14:12 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.curvi;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;

import com.memoire.bu.*;
import com.memoire.fu.FuLib;

import org.fudaa.dodico.corba.curvi.*;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.objet.IPersonne;

import org.fudaa.dodico.curvi.DCalculCurvi;

import org.fudaa.ebli.calque.*;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMaillage;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrPolygone;
import org.fudaa.ebli.palette.BPaletteCouleurSimple;
import org.fudaa.ebli.ressource.EbliResource;
import org.fudaa.ebli.trace.TracePoint;

import org.fudaa.fudaa.commun.dodico.FudaaDodicoTacheConnexion;
import org.fudaa.fudaa.commun.dodico.FudaaImplementation;
import org.fudaa.fudaa.commun.impl.FudaaFilleRapport;
import org.fudaa.fudaa.ressource.FudaaResource;
/**
 * L'implementation du client Curvi.
 *
 * @version      $Revision: 1.13 $ $Date: 2007-01-19 13:14:12 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class CurviImplementation
  extends FudaaImplementation //FudaaImplementation
{
  public static ICalculCurvi SERVEUR_CURVI;
  public static IConnexion CONNEXION_CURVI;
  public static IPersonne PERSONNE;
  // public static ICalculDunes  SERVEUR_DUNES =null;
  private FudaaFilleRapport ffRapport_;
  private CurviFilleCalques ffCalques_;
  private CurviFilleParametresINP fpINP_;
  private CurviFilleParametresASC fpASC_;
  private CurviFilleTableauResultats frTableau_;
  //  private CurviFilleSurface          frPhase_;
  // private CurviFilleSurface          frHauteur_;
  // private CurviFilleSurface          frBathymetrie_;
  //private CurviFilleSurface frDeferlement_;
  private BuPreferencesFrame preferences_;
  private BuHelpFrame aide_;
  private BArbreCalque arbre_;
  private BuAssistant assistant_;
  private BuTaskView taches_;
  String fichier_;
  SParametresCurviINP pinp_;
  SParametresCurviASC pasc_;
  SResultatsCurviCOR rcor_;
  SResultatsCurviELE rele_;
  SResultatsCurviSOL rsol_;
  GrMaillage maillage_;
  // InformationsSoftware
  private static BuInformationsSoftware isCurvi_;
  private static BuInformationsDocument idCurvi_;
  public static BuInformationsSoftware informationsSoftware() {
    if (isCurvi_ == null) {
      isCurvi_= new BuInformationsSoftware();
      isCurvi_.name= "Curvi";
      isCurvi_.version= "0.53";
      isCurvi_.date= "05-oct-2000";
      isCurvi_.rights= "Tous droits r�serv�s. CETMEF (c)1998-2000";
      isCurvi_.contact= "guillaume.desnoix@cetmef.equipement.gouv.fr";
      isCurvi_.license= "GPL2";
      isCurvi_.languages= "fr,en";
      isCurvi_.logo= CurviResource.CURVI.getIcon("logo-curvi");
      isCurvi_.banner= CurviResource.CURVI.getIcon("banner-curvi");
      // isCurvi_.ftp="ftp://172.17.250.86/";
      isCurvi_.http= "http://172.17.250.86/~desnoix/fudaa/";
      isCurvi_.man= "http://172.17.250.86/~desnoix/aide/html/";
      isCurvi_.update= "http://172.17.250.86/~desnoix/fudaa/deltas/";
      isCurvi_.authors= new String[] { "Guillaume Desnoix" };
      isCurvi_.contributors= new String[] { "Equipes Dodico, Ebli et Fudaa" };
      isCurvi_.documentors= new String[] { "Pierre Debaillon" };
      isCurvi_.translators= new String[] { "Guillaume Desnoix" };
      isCurvi_.testers= new String[] { "Pierre Debaillon" };
      isCurvi_.libraries=
        new String[] {
          "Encodeurs: Jef Poskanzer\n    (http://www.acme.com/)",
          "Expressions r�guli�res: Wes Biggs\n    (http://www.cacas.org/~wes/java/)",
          "Aelfred: David Megginson\n    (http://www.ncf.carleton.ca/~ak117/)",
          "Ic�nes 20x20: Dean S. Jones\n    (http://www.javalobby.org/jfa/projects/icons/)",
          "Ic�nes 22x22: Rivyn\n    (http://rivyn.derkarl.org/kti/)",
          "Ic�nes 24x24: Tuomas Kuosmanen\n    (http://tigert.net/)",
          };
    }
    return isCurvi_;
  }
  public BuInformationsSoftware getInformationsSoftware() {
    return informationsSoftware();
  }
  public BuInformationsDocument getInformationsDocument() {
    if (idCurvi_ == null) {
      idCurvi_= new BuInformationsDocument();
      idCurvi_.name= FudaaResource.FUDAA.getString("Etude");
      idCurvi_.version= "1";
      idCurvi_.logo= EbliResource.EBLI.getIcon("minlogo.gif");
    }
    return idCurvi_;
  }
  public void init() {
    super.init();
    try {
      final BuSplashScreen ss= getSplashScreen();
      if (ss != null) {
        ss.setText("Ajustements");
        ss.setProgression(50);
      }
      final BuInformationsSoftware il= getInformationsSoftware();
      final BuInformationsDocument id= new BuInformationsDocument();
      setTitle(il.name + " " + il.version);
      BuPrinter.INFO_LOG= il;
      BuPrinter.INFO_DOC= id;
      final BuMenuBar mb= getMainMenuBar();
      mb.addMenu(buildCalculMenu());
      final BuToolBar tb= getMainToolBar();
      tb.addSeparator();
      tb.addToolButton(
        "Connecter",
        "CONNECTER",
        FudaaResource.FUDAA.getIcon("connecter"),
        true);
      tb.addToolButton("Lancer le calcul...", "CALCULER", true);
      getApp().setEnabledForAction("OUVRIR", !(getApp() instanceof BuApplet));
      getApp().setEnabledForAction("QUITTER", true);
      getApp().setEnabledForAction("PREFERENCE", true);
      getApp().setEnabledForAction("CALCULER", SERVEUR_CURVI != null);
      getApp().removeAction("VISIBLE_LEFTCOLUMN");
      final BuMenu mf= (BuMenu)mb.getMenu("LISTE_FENETRES");
      mf.addSeparator();
      mf.addCheckBox("T�ches", "TACHE", true, true);
      mf.addCheckBox("Calques", "CALQUE", true, true);
      mb.computeMnemonics();
      if (ss != null) {
        ss.setText("Bureau et assistant");
        ss.setProgression(60);
      }
      final BuMainPanel mp= getMainPanel();
      final BuColumn lc= mp.getLeftColumn();
      final BuColumn rc= mp.getRightColumn();
      lc.setBorder(null);
      // rc.setBorder(new EmptyBorder(0,2,0,2));
      assistant_= new CurviAssistant();
      // assistant_.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
      rc.addToggledComponent(
        BuResource.BU.getString("Assistant"),
        "ASSISTANT",
        assistant_,
        this);
      taches_= new BuTaskView();
      final BuScrollPane sp= new BuScrollPane(taches_);
      sp.setPreferredSize(new Dimension(150, 80));
      rc.addToggledComponent(
        BuResource.BU.getString("T�ches"),
        "TACHE",
        sp,
        this);
      final BuMenuRecentFiles mr= (BuMenuRecentFiles)mb.getMenu("REOUVRIR");
      if (mr != null) {
        mr.setPreferences(CurviPreferences.CURVI);
        mr.setResource(CurviResource.CURVI);
      }
      mp.setLogo(il.logo);
      mp.setTaskView(taches_);
      mp.setAssistant(assistant_);
    } catch (final Throwable t) {
      t.printStackTrace();
    }
  }
  public void start() {
    super.start();
    final BuInformationsSoftware il= getInformationsSoftware();
    assistant_.actionPerformed(
      new ActionEvent(
        this,
        ActionEvent.ACTION_PERFORMED,
        "DEBUTER(" + il.name.toUpperCase() + ")"));
    BuPreferences.BU.applyOn(this);
    CurviPreferences.CURVI.applyOn(this);
    getMainPanel().getLeftColumn().setVisible(false);
    // about();
    // (new CurviConnexion(this)).start();
    assistant_.changeAttitude(
      BuAssistant.PAROLE,
      CurviResource.CURVI.getString("Bienvenue")
        + " !\n"
        + il.name
        + " "
        + il.version);
    new BuTaskOperation(this, BuResource.BU.getString("Mise en place")) {
      public void act() {
        miseEnPlace();
      }
    }
    .start();
  }
  public void miseEnPlace() {
    Thread.currentThread().setPriority(Thread.MIN_PRIORITY + 1);
    try {
      final BuMainPanel mp= getMainPanel();
      //      BuColumn    lc=mp.getLeftColumn();
      final BuColumn rc= mp.getRightColumn();
      mp.setProgression(5);
      fpINP_= new CurviFilleParametresINP();
      fpINP_.setShortcut(KeyEvent.VK_F3);
      fpINP_.setLocation(20, 20);
      addInternalFrame(fpINP_);
      installContextHelp(fpINP_.getRootPane(), "curvi/p-parametres-inp.html");
      mp.setProgression(10);
      final BGroupeCalque gc= new BGroupeCalque();
      gc.setName("cqDONNEES");
      gc.setTitle(EbliResource.EBLI.getString("Donn�es"));
      mp.setProgression(15);
      {
        final BuInformationsDocument id= getInformationsDocument();
        final BCalqueCartouche calque= new BCalqueCartouche();
        calque.setName("cqCARTOUCHE");
        calque.setTitle(FudaaResource.FUDAA.getString("Cartouche"));
        calque.setInformations(id);
        calque.setForeground(Color.black);
        calque.setBackground(new Color(255, 255, 224));
        calque.setFont(new Font("SansSerif", Font.PLAIN, 10));
        gc.add(calque);
      }
      mp.setProgression(20);
      {
        final BCalqueRosace calque= new BCalqueRosace();
        calque.setName("cqROSACE");
        calque.setTitle(FudaaResource.FUDAA.getString("Rosace"));
        calque.setForeground(Color.black);
        calque.setBackground(new Color(224, 224, 255));
        calque.setFont(new Font("SansSerif", Font.PLAIN, 10));
        gc.add(calque);
      }
      mp.setProgression(25);
      {
        final BCalqueDessin cd= new BCalqueDessin();
        cd.setName("cqDESSIN");
        cd.setTitle(FudaaResource.FUDAA.getString("Dessin"));
        final BCalqueDessinInteraction ci= new BCalqueDessinInteraction(cd);
        ci.setName("cqDESSIN_INTERACTION");
        ci.setTitle(FudaaResource.FUDAA.getString("Interaction"));
        ci.setGele(true);
        final BGroupeCalque calque= new BGroupeCalque();
        calque.setName("cqANNOTATIONS");
        calque.setTitle(FudaaResource.FUDAA.getString("Annotations"));
        calque.add(cd);
        calque.add(ci);
        gc.add(calque);
      }
      mp.setProgression(30);
      {
        final BCalqueImage calque= new BCalqueImage();
        calque.setName("cqPHARE");
        calque.setTitle("Phare");
        calque.setNW(new GrPoint(-5.0, 0.0, 0.));
        calque.setNE(new GrPoint(-2.5, 0.0, 0.));
        calque.setSE(new GrPoint(-2.5, -5.0, 0.));
        calque.setSW(new GrPoint(-5.0, -5.0, 0.));
        calque.setImage(CurviResource.CURVI.getImage("phare"));
        calque.setForeground(new Color(255, 255, 192));
        gc.add(calque);
      }
      mp.setProgression(35);
      {
        final GrBoite b= new GrBoite();
        b.ajuste(new GrPoint(-5., -5., 0.));
        b.ajuste(new GrPoint(25., 25., 0.));
        final BCalqueGrille calque= new BCalqueGrille();
        calque.setName("cqGRILLE");
        calque.setTitle(FudaaResource.FUDAA.getString("Grille"));
        calque.setBoite(b);
        calque.setPasX(0.5);
        calque.setPasY(0.5);
        calque.setForeground(Color.lightGray);
        calque.setFont(new Font("SansSerif", Font.PLAIN, 8));
        calque.setAttenue(false);
        gc.add(calque);
      }
      mp.setProgression(40);
      {
        final BCalqueDomaine calque= new BCalqueDomaine();
        calque.setName("cqDOMAINE");
        calque.setTitle(FudaaResource.FUDAA.getString("Domaine"));
        calque.setFont(new Font("SansSerif", Font.PLAIN, 8));
        calque.setAttenue(false);
        calque.setVisible(false);
        gc.add(calque);
      }
      mp.setProgression(45);
      final double[][] r= new double[][] { { 10., 10., 10. }, {
          5., 5., 5. }, {
          0., 0., 0. }, {
          0., 0., 0. }
      };
      //      BVueCalque vc=new BVueCalque();
      //      vc.setCalque(gc);
      //      vc.setBackground(Color.white);
      //      vc.setRepere(r);
      //      vc.setTaskView(taches_);
      mp.setProgression(50);
      //      arbre_=new BArbreCalque();
      //      arbre_.setCalque(gc);
      mp.setProgression(55);
      ffRapport_= new FudaaFilleRapport(getApp());
      ffRapport_.setShortcut(KeyEvent.VK_F5);
      ffRapport_.setLocation(20, fpINP_.getSize().height + 40);
      addInternalFrame(ffRapport_);
      mp.setProgression(60);
      ffCalques_= new CurviFilleCalques(gc);
      arbre_= new BArbreCalque();
      arbre_.setModel(ffCalques_.getArbreCalqueModel());
      ffCalques_.addInternalFrameListener(arbre_);
      final BVueCalque vc= ffCalques_.getVueCalque();
      //vc.setCalque(gc);
      vc.setBackground(Color.white);
      vc.setRepere(r);
      vc.setTaskView(taches_);
      ffCalques_.setShortcut(KeyEvent.VK_F4);
      ffCalques_.setLocation(170, 20);
      addInternalFrame(ffCalques_);
      mp.setProgression(65);
      final JScrollPane sp= new JScrollPane(arbre_);
      sp.setSize(150, 150);
      rc.addToggledComponent(
        EbliResource.EBLI.getString("Calques"),
        "CALQUE",
        sp,
        this);
      // mp.doLayout();
      // mp.validate();
      mp.setProgression(70);
      pasc_= creeSParametresCurviASC();
      getMainMenuBar().getMenu("RELEVE").setEnabled(true);
      setEnabledForAction("TABLEAU_RELEVE", true);
      setEnabledForAction("CALQUE_RELEVE", true);
      mp.setProgression(75);
      assistant_.addEmitters((Container)getApp());
      assistant_.changeAttitude(BuAssistant.ATTENTE, "");
      mp.setProgression(80);
      ffRapport_.insereEnteteEtude(getInformationsDocument());
      installContextHelp(ffRapport_.getRootPane(), "fudaa/p-rapport.html");
     // cmdConnecter();
    } catch (final Exception ex) {
      ex.printStackTrace();
    }
  }
  // Actions
  public void actionPerformed(final ActionEvent _evt) {
    final String action= _evt.getActionCommand();
    System.err.println("ACTION=" + action);
    if (action.equals("OUVRIR")) {
      cmdOuvrir();
    } else if (action.equals("PREFERENCE")) {
      cmdPreferences();
    } else if (action.equals("PARAMETRE_GENERAL")) {
      cmdParametreGeneral();
    } else if (action.equals("PARAMETRE_HOULE")) {
      cmdParametreHoule();
    } else if (action.equals("TABLEAU_RELEVE")) {
      cmdTableauReleve();
    } else if (action.equals("CALQUE_RELEVE")) {
      cmdCalqueReleve();
    } else if (action.equals("CALCULER")) {
      cmdCalculer();
    } else if (action.equals("TABLEAU")) {
      cmdTableau();
    } else if (action.equals("IMAGE_PHASE")) {
      cmdPhase();
    } else if (action.equals("IMAGE_HAUTEUR")) {
      cmdHauteur();
    } else if (action.equals("IMAGE_BATHYMETRIE")) {
      cmdBathymetrie();
    } else if (action.equals("IMAGE_DEFERLEMENT")) {
      cmdDeferlement();
    } else if (action.equals("PHOTOGRAPHIE_VUECALQUE")) {
      System.out.println("Photographie vue calque");
      ffRapport_.insereIcone(
        new ImageIcon(ffCalques_.getVueCalque().getImageCache()),
        "Vue " + FuLib.date());
    } else if (
      action.equals("ASSISTANT")
        || action.equals("TACHE")
        || action.equals("CALQUE")) {
      final BuColumn rc= getMainPanel().getRightColumn();
      rc.toggleComponent(action);
      setCheckedForAction(action, rc.isToggleComponentVisible(action));
    } else {
      super.actionPerformed(_evt);
    }
  }
  public void displayURL(String _url) {
    if (BuPreferences.BU.getIntegerProperty("browser.type", 1) != 1) {
      if (_url == null) {
        final BuInformationsSoftware il= getInformationsSoftware();
        _url= il.http;
      }
      BuBrowserControl.displayURL(_url);
    } else {
      if (aide_ == null) {
        aide_= new BuHelpFrame(this);
      }
      addInternalFrame(aide_);
      aide_.setDocumentUrl(_url);
    }
  }
  // Menu Calcul
  public static BuMenu buildReleveMenu() {
    final BuMenu r=
      new BuMenu(
        CurviResource.CURVI.getString("Relev� bathym�trique"),
        "RELEVE");
    r.addMenuItem(
      FudaaResource.FUDAA.getString("Tableau"),
      "TABLEAU_RELEVE",
      false);
    r.addMenuItem(
      FudaaResource.FUDAA.getString("Calque"),
      "CALQUE_RELEVE",
      false);
    return r;
  }
  public static BuMenu buildCalculMenu() {
    final BuMenu r= new BuMenu(FudaaResource.FUDAA.getString("Calcul"), "CALCUL");
    r.addMenuItem(
      FudaaResource.FUDAA.getString("Lancer le calcul..."),
      "CALCULER",
      false);
    r.addSeparator(FudaaResource.FUDAA.getString("Param�tres"));
    r.addMenuItem(
      FudaaResource.FUDAA.getString("G�n�raux"),
      "PARAMETRE_GENERAL",
      true);
    r.addMenuItem(
      CurviResource.CURVI.getString("Houle"),
      "PARAMETRE_HOULE",
      CurviResource.CURVI.getIcon("houle"),
      true);
    r.addSubMenu(buildReleveMenu(), false);
    // "RELEVE",CurviResource.CURVI.getIcon("releve")
    r.addSeparator(FudaaResource.FUDAA.getString("R�sultats"));
    r.addMenuItem(FudaaResource.FUDAA.getString("Tableau"), "TABLEAU", false);
    r.addMenuItem(CurviResource.CURVI.getString("Phase"), "IMAGE_PHASE", false);
    r.addMenuItem(
      CurviResource.CURVI.getString("Hauteur"),
      "IMAGE_HAUTEUR",
      false);
    r.addMenuItem(
      CurviResource.CURVI.getString("Bathym�trie"),
      "IMAGE_BATHYMETRIE",
      false);
    r.addMenuItem(
      CurviResource.CURVI.getString("D�ferlement"),
      "IMAGE_DEFERLEMENT",
      false);
    // r.addSeparator("Dunes");
    // r.addMenuItem("Test"             ,"CALCULER_DUNES"   ,true );
    r.addSeparator("Rapport");
    r.addMenuItem("Photographie", "PHOTOGRAPHIE_VUECALQUE", true);
    return r;
  }
  // Commandes
  public void cmdOuvrir() {
    if (getApp() instanceof BuApplet) {
      return;
    }
    final JFileChooser chooser= new JFileChooser();
    chooser.setFileHidingEnabled(true);
    chooser.resetChoosableFileFilters();
    final String[] exts= new String[] { "asc", "ASC" };
    final BuFileFilter filter= new BuFileFilter(exts, "Relev�s bathym�triques");
    chooser.setFileFilter(filter);
    String r;
    final int returnVal= chooser.showOpenDialog((BuApplication)getApp());
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      r= chooser.getSelectedFile().getPath(); // .getName();
      System.out.println("You chose to open this file: " + r);
    } else {
      r= null;
    }
    if (r != null) {
      /*
      AlmaTacheOuverture tache=new AlmaTacheOuverture(this,r);
      tache.setPriority(Thread.MIN_PRIORITY);
      tache.start();
      */
      final BuMenuBar mb= getMainMenuBar();
      mb.addRecentFile(r, "texte");
      mb.getMenu("REOUVRIR").setEnabled(true);
    }
  }
  public void cmdPreferences() {
    final BuMainPanel mp= getMainPanel();
    mp.setProgression(5);
    if (preferences_ == null) {
      preferences_= new BuPreferencesFrame();
      mp.setProgression(15);
      preferences_.addTab(new BuLookPreferencesPanel(this));
      mp.setProgression(25);
      preferences_.addTab(new BuDesktopPreferencesPanel(this));
      mp.setProgression(35);
      preferences_.addTab(new BuLanguagePreferencesPanel(this));
      mp.setProgression(45);
      preferences_.addTab(new BuBrowserPreferencesPanel(this));
      mp.setProgression(50);
      if (!(getApp() instanceof BuApplet)) {
        preferences_.addTab(new BuDirWinPreferencesPanel(this));
        mp.setProgression(65);
      }
      preferences_.addTab(new BuUserPreferencesPanel(this));
      mp.setProgression(75);
      preferences_.addTab(new CurviPreferencesPanel(this));
      mp.setProgression(85);
    }
    mp.setProgression(100);
    addInternalFrame(preferences_);
    mp.setProgression(0);
  }
  public void cmdParametreGeneral() {
    activateInternalFrame(fpINP_);
    fpINP_.setSelectedIndex(0);
  }
  public void cmdParametreHoule() {
    activateInternalFrame(fpINP_);
    fpINP_.setSelectedIndex(1);
  }
  public void cmdCalqueReleve() {
    final BuTask t=
      new CurviTacheOperation(
        this,
        "Calque du relev� bathym�trique",
        "oprCalqueReleve");
    t.start();
  }
  public void oprCalqueReleve() {
    if (pasc_ == null) {
      return;
    }
    {
      final BCalquePoint calque= new BCalquePoint();
      calque.setName("cqRELEVE_BATHYMETRIQUE");
      calque.setTitle(CurviResource.CURVI.getString("Relev� bathym�trique"));
      calque.setForeground(Color.red);
      calque.setBackground(new Color(255, 255, 224));
      calque.setFont(new Font("SansSerif", Font.PLAIN, 10));
      // calque.setEtiquettePleine(true);
      calque.setTypePoint(TracePoint.CROIX);
      // calque.setTypePoint(TracePoint.ICONE);
      // calque.setIcon(CurviResource.CURVI.getIcon("drapeau"));
      for (int i= 0; i < pasc_.lignes.length; i++) {
        final GrPoint p=
          new GrPoint(pasc_.lignes[i].x, pasc_.lignes[i].y, pasc_.lignes[i].z);
        calque.ajoute(p); // ,""+pasc_.lignes[i].z);
        setProgression(50 * i / pasc_.lignes.length);
        // System.err.println(""+i+" : "+pasc_.lignes[i].z);
      }
      ajouteCalque(calque);
    }
    {
      final BPaletteCouleurSimple pal= new BPaletteCouleurSimple();
      // if(CurviPreferences.CURVI.isDifferentesPalettes())
      {
        pal.setEspace(true);
        pal.setCouleurMin(Color.blue);
        pal.setCouleurMax(Color.white);
        // pal.setPaliers(16);
      }
      final BCalqueGrilleReguliere calque= new BCalqueGrilleReguliere();
      calque.setName("cqFONDS_MARINS");
      calque.setTitle(CurviResource.CURVI.getString("Fonds marins"));
      final int nbx= pasc_.nbMaillesXI;
      final int nby= pasc_.nbMaillesETA;
      final double[][] vvz= new double[nbx + 1][nby + 1];
      for (int i= 0; i <= nbx; i++) {
        setProgression(50 + 50 * i / nbx);
        for (int j= 0; j <= nby; j++) {
          vvz[i][j]= pasc_.lignes[j * (nbx + 1) + i].z;
        }
      }
      final GrPoint p1= new GrPoint(pasc_.lignes[0].x, pasc_.lignes[0].y, 0.);
      final GrPoint p2= new GrPoint(pasc_.lignes[nbx].x, pasc_.lignes[nbx].y, 0.);
      final GrPoint p3=
        new GrPoint(
          pasc_.lignes[(nbx + 1) * (nby + 1) - 1].x,
          pasc_.lignes[(nbx + 1) * (nby + 1) - 1].y,
          0.);
      final GrPoint p4=
        new GrPoint(
          pasc_.lignes[(nbx + 1) * (nby + 1) - 1 - nbx].x,
          pasc_.lignes[(nbx + 1) * (nby + 1) - 1 - nbx].y,
          0.);
      final GrPolygone rect= new GrPolygone();
      rect.sommets_.ajoute(p1);
      rect.sommets_.ajoute(p2);
      rect.sommets_.ajoute(p3);
      rect.sommets_.ajoute(p4);
      calque.setRectangle(rect);
      calque.setValeurs(vvz);
      calque.setPaletteCouleur(pal);
      ajouteCalque(calque);
    }
    setEnabledForAction("CALQUE_RELEVE", false);
  }
  public void cmdTableauReleve() {
    final BuTask t=
      new CurviTacheOperation(
        this,
        CurviResource.CURVI.getString("Tableau")
          + " - "
          + CurviResource.CURVI.getString("Relev� bathym�trique"),
        "oprTableauReleve");
    t.start();
  }
  public void oprTableauReleve() {
    if (pasc_ == null) {
      return;
    }
    fpASC_=
      new CurviFilleParametresASC(
        this,
        CurviResource.CURVI.getString("Tableau")
          + " - "
          + CurviResource.CURVI.getString("Relev� bathym�trique"),
        pasc_);
    addInternalFrame(fpASC_);
  }
  // ASC
  protected SParametresCurviASC creeSParametresCurviASC() {
    final SParametresCurviASC r= new SParametresCurviASC();
    r.nbMaillesXI= 10;
    r.nbMaillesETA= 10;
    int i, j, l;
    l= (r.nbMaillesXI + 1) * (r.nbMaillesETA + 1);
    r.lignes= new SParametresLigneCurviASC[l];
    for (i= 0; i <= r.nbMaillesXI; i++) {
      for (j= 0; j <= r.nbMaillesETA; j++) {
        final double x= 30. * i / r.nbMaillesXI;
        final double y= 30. * j / r.nbMaillesETA;
        double z=
          -0.20
            - x / 20.
            - y / 20.
            - Math.abs(2. * Math.sin(x / 10.) * Math.sin(x / 20. + y / 10.));
        z= Math.round(z * 1000.) / 1000.;
        r.lignes[i + j * (r.nbMaillesXI + 1)]=
          new SParametresLigneCurviASC(x, y, z);
      }
    }
    /*
    getMainMenuBar().getMenu("RELEVE").setEnabled(true);
    setEnabledForAction("TABLEAU_RELEVE",true);
    setEnabledForAction("CALQUE_RELEVE" ,true);
    */
    return r;
  }
  // Calcul
  public void cmdCalculer() {
    if (!isConnected()) {
      new BuDialogError(
        getApp(),
        isCurvi_,
        "vous n'etes pas connect� � un serveur!")
        .activate();
      return;
    }
    final BuTask t= new CurviTacheCalcul(this);
    t.setTaskView(getMainPanel().getTaskView());
    t.setPriority(Thread.MIN_PRIORITY);
    t.start();
  }
  public void oprCalculer() {
    long avant, apres;
    long dureeEstimee, dureeReelle, dureeTransfert, dureeTotale;
    setProgression(0);
    setMessage("");
    setEnabledForAction("TABLEAU", false);
    setEnabledForAction("IMAGE_PHASE", false);
    setEnabledForAction("IMAGE_HAUTEUR", false);
    setEnabledForAction("IMAGE_BATHYMETRIE", false);
    setEnabledForAction("IMAGE_DEFERLEMENT", false);
    setEnabledForAction("PARAMETRE", false);
    setEnabledForAction("CALCULER", false);
    BuLib.setRecursiveEnabled(fpINP_, false);
    /*
    if(frTableau_    !=null) mp.removeInternalFrame(frTableau_    );
    if(frPhase_      !=null) mp.removeInternalFrame(frPhase_      );
    if(frHauteur_    !=null) mp.removeInternalFrame(frHauteur_    );
    if(frBathymetrie_!=null) mp.removeInternalFrame(frBathymetrie_);
    if(frDeferlement_!=null) mp.removeInternalFrame(frDeferlement_);
    */
    setProgression(5);
    setMessage("Initialisation de INP");
    pinp_= fpINP_.creeSParametresCurviINP();
    setMessage("Initialisation de ASC");
    // FichierINP pinp_=new FichierINP(10.,0.01,0.,90.,10,
    // 10,-30.,-10.,30.,-10.,30.,30.,2,-1,0);
    avant= System.currentTimeMillis();
    fichier_= fpINP_.getNomProjet();
    fichier_= fichier_.replace(' ', '_');
    if (fichier_.length() > 8) {
      fichier_= fichier_.substring(0, 8);
    }
    System.out.println("Param�tres");
    final IParametresCurvi ip=
      IParametresCurviHelper.narrow(SERVEUR_CURVI.parametres(CONNEXION_CURVI));
    setProgression(7);
    //    ip.fichier(fichier_);
    ip.parametresINP(pinp_);
    ip.parametresASC(pasc_);
    apres= System.currentTimeMillis();
    dureeTransfert= (apres - avant) / 1000L;
    setMessage("Dur�e de transfert des param�tres: " + dureeTransfert + " s");
    setProgression(16);
    setMessage("INP initialis�");
    setProgression(9);
    dureeEstimee= SERVEUR_CURVI.dureeEstimee(CONNEXION_CURVI);
    setMessage("Dur�e estim�e : " + dureeEstimee + " s");
    setProgression(10);
    if (CurviPreferences.CURVI.isDialoguesCalcul()) {
      final String tm=
        "Calcul pr�t � �tre lanc�\n"
          + "Dur�e estim�e du calcul: "
          + dureeEstimee
          + " s\n"
          + "Dur�e estim�e totale: "
          + (dureeEstimee + 20)
          + " s\n";
      final BuDialogMessage dm=
        new BuDialogMessage(this, getInformationsSoftware(), tm);
      dm.activate();
    }
    avant= apres;
    SERVEUR_CURVI.calcul(CONNEXION_CURVI);
    apres= System.currentTimeMillis();
    System.err.println(
      "Calcul   : " + (apres - avant) + " ms [" + fichier_ + "]");
    setProgression(30);
    setMessage("Calcul termin�");
    dureeReelle= (apres - avant) / 1000L;
    setMessage("Dur�e r�elle du calcul: " + dureeReelle + " s");
    System.out.println("R�sultats");
    final IResultatsCurvi ir=
      IResultatsCurviHelper.narrow(SERVEUR_CURVI.resultats(CONNEXION_CURVI));
    maillage_= null;
    rcor_= ir.resultatsCurviCOR();
    System.out.println(rcor_.lignes.length + " noeuds");
    setProgression(55);
    rele_= ir.resultatsCurviELE();
    System.out.println(rele_.lignes.length + " elements");
    setProgression(75);
    rsol_= ir.resultatsCurviSOL();
    System.out.println(rsol_.lignes.length + " solutions");
    setProgression(95);
    setMessage("");
    setEnabledForAction("TABLEAU", true);
    setEnabledForAction("IMAGE_PHASE", true);
    setEnabledForAction("IMAGE_HAUTEUR", true);
    setEnabledForAction("IMAGE_BATHYMETRIE", true);
    setEnabledForAction("IMAGE_DEFERLEMENT", true);
    apres= System.currentTimeMillis();
    dureeTotale= (apres - avant) / 1000L;
    dureeTransfert += (dureeTotale - dureeReelle);
    try {
      Thread.sleep(500);
    } catch (final Exception ex) {}
    setProgression(100);
    if (CurviPreferences.CURVI.isDialoguesCalcul()) {
      final String tm=
        "Calcul termin�\nDur�e estim�e du calcul: "
          + dureeEstimee
          + " s\n"
          + "Dur�e r�elle du calcul: "
          + dureeReelle
          + " s\n"
          + "Dur�e de transfert: "
          + dureeTransfert
          + " s\n"
          + "Dur�e totale: "
          + dureeTotale
          + " s\n";
      final BuDialogMessage dm=
        new BuDialogMessage(this, getInformationsSoftware(), tm);
      dm.activate();
    }
    setEnabledForAction("PARAMETRE", true);
    setEnabledForAction("CALCULER", true);
    BuLib.setRecursiveEnabled(fpINP_, true);
    setProgression(0);
  }
  public void cmdTableau() {
    final BuTask t= new CurviTacheOperation(this, "Tableau", "oprTableau");
    t.start();
  }
  public void oprTableau() {
    if ((rcor_ == null) || (rsol_ == null)) {
      return;
    }
    frTableau_=
      new CurviFilleTableauResultats(
        this,
        CurviResource.CURVI.getString("Tableau")
          + " - "
          + CurviResource.CURVI.getString("R�sultats"),
        rcor_,
        rsol_);
    addInternalFrame(frTableau_);
  }
  protected GrMaillage construitMaillage(
    final SResultatsCurviCOR _rcor,
    final SResultatsCurviELE _rele) {
    setMessage("Construction du maillage");
    final GrMaillage r= new GrMaillage();
    int i;
    for (i= 0; i < _rcor.lignes.length; i++) {
      final GrPoint p= new GrPoint(_rcor.lignes[i].x, _rcor.lignes[i].y, 0.);
      r.noeuds_.ajoute(p);
      setProgression(100 * (i + 1) / _rcor.lignes.length);
    }
    for (i= 0; i < _rele.lignes.length; i++) {
      final int[] c= new int[4];
      c[0]= _rele.lignes[i].n1 - 1;
      c[1]= _rele.lignes[i].n2 - 1;
      c[2]= _rele.lignes[i].n3 - 1;
      c[3]= _rele.lignes[i].n4 - 1;
      r.connectivites_.add(c);
      setProgression(100 * (i + 1) / _rele.lignes.length);
    }
    return r;
  }
  protected double[] construitValeurs(final SResultatsCurviSOL _rsol, final int _colonne) {
    setMessage("Construction des valeurs");
    int i, n;
    double[] r;
    n= _rsol.lignes.length;
    r= new double[n];
    for (i= 0; i < n; i++) {
      r[i]= _rsol.lignes[i].v[_colonne];
      setProgression(100 * (i + 1) / n);
    }
    return r;
  }
  protected double[] construitRatios(
    final SResultatsCurviSOL _rsol,
    final int _colonne1,
    final int _colonne2) {
    setMessage("Construction des ratios");
    int i, n;
    double[] r;
    n= _rsol.lignes.length;
    r= new double[n];
    for (i= 0; i < n; i++) {
      r[i]=
        (rsol_.lignes[i].v[_colonne1] / rsol_.lignes[i].v[_colonne2] > 0.78)
          ? 1.
          : 0.;
      setProgression(100 * (i + 1) / n);
    }
    return r;
  }
  protected void ajouteCalque(final BCalque _calque) {
    final BVueCalque vc= ffCalques_.getVueCalque();
    final BCalque gc= vc.getCalque();
    gc.add(_calque);
    vc.invalidate();
    ffCalques_.validate();
    ffCalques_.repaint();
    arbre_.refresh();
  }
  public void cmdPhase() {
    final BuTask t= new CurviTacheOperation(this, "Phase", "oprPhase");
    t.start();
  }
  public void oprPhase() {
    if ((rcor_ == null) || (rsol_ == null)) {
      return;
    }
    if (maillage_ == null) {
      maillage_= construitMaillage(rcor_, rele_);
    }
    final double[] valeurs= construitValeurs(rsol_, 0);
    final BPaletteCouleurSimple pal= new BPaletteCouleurSimple();
    if (CurviPreferences.CURVI.isDifferentesPalettes()) {
      pal.setEspace(true);
      pal.setCouleurMin(Color.black);
      pal.setCouleurMax(Color.red);
    }
    final BCalqueMaillage calque= new BCalqueMaillage();
    calque.setName("cqPHASE_" + fichier_.toUpperCase());
    calque.setTitle(CurviResource.CURVI.getString("Phase") + " - " + fichier_);
    calque.setMaillage(maillage_);
    calque.setValeurs(valeurs);
    calque.setPaletteCouleur(pal);
    ajouteCalque(calque);
    setEnabledForAction("IMAGE_PHASE", false);
  }
  public void cmdHauteur() {
    final BuTask t= new CurviTacheOperation(this, "Hauteur", "oprHauteur");
    t.start();
  }
  public void oprHauteur() {
    if ((rcor_ == null) || (rsol_ == null)) {
      return;
    }
    if (maillage_ == null) {
      maillage_= construitMaillage(rcor_, rele_);
    }
    final double[] valeurs= construitValeurs(rsol_, 1);
    final BPaletteCouleurSimple pal= new BPaletteCouleurSimple();
    if (CurviPreferences.CURVI.isDifferentesPalettes()) {
      pal.setEspace(true);
      pal.setCouleurMin(Color.black);
      pal.setCouleurMax(Color.green);
      // pal.setPaliers(16);
    }
    final BCalqueMaillage calque= new BCalqueMaillage();
    calque.setName("cqHAUTEUR_" + fichier_.toUpperCase());
    calque.setTitle(
      CurviResource.CURVI.getString("Hauteur") + " - " + fichier_);
    calque.setMaillage(maillage_);
    calque.setValeurs(valeurs);
    calque.setPaletteCouleur(pal);
    ajouteCalque(calque);
    setEnabledForAction("IMAGE_HAUTEUR", false);
  }
  public void cmdBathymetrie() {
    final BuTask t= new CurviTacheOperation(this, "Bathymetrie", "oprBathymetrie");
    t.start();
  }
  public void oprBathymetrie() {
    if ((rcor_ == null) || (rsol_ == null)) {
      return;
    }
    if (maillage_ == null) {
      maillage_= construitMaillage(rcor_, rele_);
    }
    final double[] valeurs= construitValeurs(rsol_, 2);
    final BPaletteCouleurSimple pal= new BPaletteCouleurSimple();
    if (CurviPreferences.CURVI.isDifferentesPalettes()) {
      pal.setEspace(true);
      pal.setCouleurMin(Color.black);
      pal.setCouleurMax(Color.blue);
    }
    final BCalqueMaillage calque= new BCalqueMaillage();
    calque.setName("cqBATHYMETRIE_" + fichier_.toUpperCase());
    calque.setTitle(
      CurviResource.CURVI.getString("Bathym�trie") + " - " + fichier_);
    calque.setMaillage(maillage_);
    calque.setValeurs(valeurs);
    calque.setPaletteCouleur(pal);
    ajouteCalque(calque);
    setEnabledForAction("IMAGE_BATHYMETRIE", false);
  }
  public void cmdDeferlement() {
    final BuTask t= new CurviTacheOperation(this, "D�ferlement", "oprDeferlement");
    t.start();
  }
  public void oprDeferlement() {
    if ((rcor_ == null) || (rsol_ == null)) {
      return;
    }
    if (maillage_ == null) {
      maillage_= construitMaillage(rcor_, rele_);
    }
    final double[] valeurs= construitRatios(rsol_, 1, 2);
    final BPaletteCouleurSimple pal= new BPaletteCouleurSimple();
    pal.setEspace(true);
    pal.setCouleurMin(Color.magenta);
    pal.setCouleurMax(Color.magenta.brighter());
    pal.setPaliers(2);
    final BCalqueMaillage calque= new BCalqueMaillage();
    calque.setName("cqDEFERLEMENT_" + fichier_.toUpperCase());
    calque.setTitle(
      CurviResource.CURVI.getString("D�ferlement") + " - " + fichier_);
    calque.setMaillage(maillage_);
    calque.setValeurs(valeurs);
    calque.setPaletteCouleur(pal);
    ajouteCalque(calque);
    setEnabledForAction("IMAGE_DEFERLEMENT", false);
  }
  // Suivi
  public void setMessage(final String _s) {
    getMainPanel().setMessage(_s);
  }
  public void setProgression(final int _v) {
    getMainPanel().setProgression(_v);
    final Thread t= Thread.currentThread();
    if (t instanceof BuTask) {
      ((BuTask)t).setProgression(_v);
    }
  }
  // Test Dunes
  /*
  public void testDunes()
  {
    System.out.println("Param�tres");
    int i,j;

    CParametresDunes a=new CParametresDunes();
    System.out.println("A="+a.enChaine());

    int NP=32;
    IPoint[] p=new IPoint[NP];
    for(i=0;i<NP;i++)
      p[i]=new CPoint(150.+100.*Math.cos(2.*Math.PI*i/NP),
  	      150.+100.*Math.sin(2.*Math.PI*i/NP));

    a.points(p);
    System.out.println("A="+a.enChaine());

    SERVEUR_DUNES.parametres(a);
    SERVEUR_DUNES.typeElementDunes(LTypeElementDunes.T3);
    SERVEUR_DUNES.optionP(true);
    SERVEUR_DUNES.optionQ(true);
    System.out.println("C="+SERVEUR_DUNES.enChaine());

    System.out.println("Duree: "+SERVEUR_DUNES.dureeEstimee());
    SERVEUR_DUNES.calcule();

    IResultatsDunes r=IResultatsDunesHelper.narrow(SERVEUR_DUNES.resultats());
    System.out.println("R="+r.enChaine());

    IMaillage m=r.maillage();
    System.out.println("M="+m.enChaine());

    IElement[] elts=m.elements();
    System.out.println(elts[0].enChaine());

    INoeud[] nds=elts[0].noeuds();
    for(i=0;i<nds.length;i++)
      System.out.println(""+nds[i].enChaine()+", "+nds[i].point().enChaine());

    {
      BCalquePolygone calque=new BCalquePolygone();
      calque.setName("cqMAILLAGE");
      calque.setTitle(CurviResource.CURVI.getString("Maillage"));
      for(i=0;i<elts.length;i++)
      {
  IPoint[]   pe=elts[i].points();
  GrPolygone qe=new GrPolygone();
  for(j=0;j<pe.length;j++)
  {
    double[] ce=pe[j].coordonnees();
    qe.sommets.ajoute(new GrPoint(ce[0],ce[1],0.));
    calque.ajoute(qe);
  }
      }
      calque.setForeground(Color.blue);

      ajouteCalque(calque);
    }
  }
  */
  public void exit() {
    closeConnexions();
    super.exit();
  }
  public void finalize() {
    closeConnexions();
  }
  /**
   * @see org.fudaa.fudaa.commun.impl.FudaaCommonImplementation#getApplicationPreferences()
   */
  public BuPreferences getApplicationPreferences() {
    return CurviPreferences.CURVI;
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#clearVariables()
   */
  protected void clearVariables() {
  CONNEXION_CURVI=null;
  SERVEUR_CURVI=null;
  }
  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheConnexionMap()
   */
  protected FudaaDodicoTacheConnexion[] getTacheConnexionMap() {
    final FudaaDodicoTacheConnexion c=new FudaaDodicoTacheConnexion(SERVEUR_CURVI,CONNEXION_CURVI);
    return new FudaaDodicoTacheConnexion[]{c};
  }
  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheDelegateClass()
   */
  protected Class[] getTacheDelegateClass() {
    return new Class[]{DCalculCurvi.class};
  }
  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#initConnexions(java.util.Map)
   */
  protected void initConnexions(final Map _r) {
  final FudaaDodicoTacheConnexion c=(FudaaDodicoTacheConnexion)_r.get(DCalculCurvi.class);
  CONNEXION_CURVI=c.getConnexion();
  SERVEUR_CURVI=ICalculCurviHelper.narrow(c.getTache());
  }
}
