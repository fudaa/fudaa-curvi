/*
 * @file         CurviFilleCalques.java
 * @creation     1999-01-13
 * @modification $Date: 2006-09-19 15:02:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.curvi;
import javax.swing.JComponent;

import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuPopupButton;

import org.fudaa.ebli.calque.BCalque;
import org.fudaa.ebli.calque.EbliFilleCalques;
import org.fudaa.ebli.palette.BPaletteForme;
import org.fudaa.ebli.palette.BPaletteProprietesSurface;
import org.fudaa.ebli.ressource.EbliResource;
/**
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:02:25 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class CurviFilleCalques extends EbliFilleCalques {
  private BuPopupButton pbProprietes;
  private BuPopupButton pbFormes;
  public CurviFilleCalques(final BCalque _vc) {
    //super(_vc,_arbre);
    super(_vc);
    //addInternalFrameListener(_arbre);
    final BPaletteProprietesSurface srcprop= new BPaletteProprietesSurface();
    srcprop.addPropertyChangeListener(getArbreCalqueModel());
    pbProprietes= new BuPopupButton("Propriétés", srcprop);
    pbProprietes.setToolTipText("Propriétés d'affichage du maillage");
    pbProprietes.setIcon(EbliResource.EBLI.getIcon("remplissage"));
    final BPaletteForme srcfrm= new BPaletteForme();
    srcfrm.addPropertyChangeListener(getArbreCalqueModel());
    pbFormes= new BuPopupButton("Formes", srcfrm);
    pbFormes.setToolTipText("Palette de formes");
    pbFormes.setIcon(EbliResource.EBLI.getIcon("paletteforme"));
  }
  // BuInternalFrame
  public JComponent[] getSpecificTools() {
    pbProprietes.setDesktop((BuDesktop)getDesktopPane());
    pbFormes.setDesktop((BuDesktop)getDesktopPane());
    final JComponent[] s= super.getSpecificTools();
    final JComponent[] r= new JComponent[s.length + 4];
    for (int i= 0; i < s.length; i++) {
      r[i]= s[i];
    }
    final int i= s.length;
    r[i]= null;
    r[i + 1]= pbProprietes;
    r[i + 2]= null;
    r[i + 3]= pbFormes;
    return r;
  }
}
