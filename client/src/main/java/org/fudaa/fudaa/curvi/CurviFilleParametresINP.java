/*
 * @file         CurviFilleParametresINP.java
 * @creation     1999-01-13
 * @modification $Date: 2006-09-19 15:02:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.curvi;
import java.awt.Font;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.curvi.SParametresCurviINP;

import org.fudaa.fudaa.commun.FudaaLib;
/**
 * @version      $Revision: 1.7 $ $Date: 2006-09-19 15:02:25 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class CurviFilleParametresINP extends BuInternalFrame {
  private JTabbedPane tpMain;
  private JComponent content_;
  private BuTextField tf_nom_projet;
  private BuTextField tf_periode_houle;
  private BuTextField tf_hauteur_houle;
  private BuTextField tf_hauteur_mer;
  private BuTextField tf_incidence_houle;
  private BuTextField tf_nbmaillesXI;
  private BuTextField tf_nbmaillesETA;
  private BuTextField tf_xd1, tf_yd1, tf_xd2, tf_yd2, tf_xd3, tf_yd3;
  private JComboBox ch_maillage;
  private JComboBox ch_cas;
  private JCheckBox cb_reprise_vag;
  public CurviFilleParametresINP() {
    super("", false, false, false, true);
    setName("ifPARAMETRES_INP");
    int n;
    final JPanel pnGenerales= new JPanel();
    final BuGridLayout loGenerales= new BuGridLayout();
    loGenerales.setColumns(3);
    loGenerales.setHgap(5);
    loGenerales.setVgap(5);
    pnGenerales.setLayout(loGenerales);
    pnGenerales.setBorder(new EmptyBorder(5, 5, 5, 5));
    n= 0;
    tf_nom_projet= BuTextField.createIdField();
    ch_maillage= new JComboBox();
    ch_cas= new JComboBox();
    cb_reprise_vag= new JCheckBox();
    tf_nom_projet.setName("tfNOMPROJET");
    ch_maillage.setName("chMAILLAGE");
    ch_cas.setName("chCAS");
    cb_reprise_vag.setName("cbREPRISEVAG");
    tf_nom_projet.setText("etude");
    cb_reprise_vag.setText(CurviResource.CURVI.getString("activ�e"));
    ch_maillage.addItem("Type Vag");
    ch_maillage.addItem("Type Zhang");
    ch_maillage.addItem("Type Do�kas");
    ch_cas.addItem(CurviResource.CURVI.getString("R�el Bathycad"));
    ch_cas.addItem(CurviResource.CURVI.getString("R�el CEF"));
    ch_cas.addItem(CurviResource.CURVI.getString("Pente inclin�e 10% selon Y"));
    ch_cas.addItem(CurviResource.CURVI.getString("Pente inclin�e 10% selon X"));
    ch_cas.addItem(CurviResource.CURVI.getString("Bosse de Delft"));
    ch_cas.addItem(CurviResource.CURVI.getString("Double bosse de Delft"));
    ch_cas.addItem(CurviResource.CURVI.getString("Fond plat -3m"));
    ch_cas.addItem(CurviResource.CURVI.getString("Brise-lame P.Pechon LNH"));
    ch_cas.addItem(CurviResource.CURVI.getString("Brise-lame SOGREAH"));
    ch_cas.addItem(
      CurviResource.CURVI.getString("Bosse parabolique sur fond plat"));
    ch_cas.addItem(CurviResource.CURVI.getString("Talus � 4%"));
    ch_maillage.setFont(new Font("SansSerif", Font.PLAIN, 10));
    ch_cas.setFont(new Font("SansSerif", Font.PLAIN, 10));
    tf_nom_projet.setColumns(8);
    ch_maillage.setSelectedIndex(2);
    ch_cas.setSelectedIndex(4);
    cb_reprise_vag.setEnabled(false);
    pnGenerales.add(
      new JLabel(CurviResource.CURVI.getString("Nom du projet:"), SwingConstants.RIGHT),
      n++);
    pnGenerales.add(tf_nom_projet, n++);
    pnGenerales.add(new JLabel(".inp", SwingConstants.LEFT), n++);
    /*
    pnGenerales.add(new JLabel("("
    	       +CurviResource.CURVI.getString("fichier")
    	       +".inp)",SwingConstants.LEFT)  ,n++);
    */
    pnGenerales.add(
      new JLabel(CurviResource.CURVI.getString("Maillage:"), SwingConstants.RIGHT),
      n++);
    pnGenerales.add(ch_maillage, n++);
    pnGenerales.add(new JLabel(" ", SwingConstants.LEFT), n++);
    pnGenerales.add(
      new JLabel(CurviResource.CURVI.getString("Cas:"), SwingConstants.RIGHT),
      n++);
    pnGenerales.add(ch_cas, n++);
    pnGenerales.add(new JLabel(" ", SwingConstants.LEFT), n++);
    pnGenerales.add(
      new JLabel(CurviResource.CURVI.getString("Reprise:"), SwingConstants.RIGHT),
      n++);
    pnGenerales.add(cb_reprise_vag, n++);
    pnGenerales.add(new JLabel(" ", SwingConstants.LEFT), n++);
    final JPanel pnHoule= new JPanel();
    final BuGridLayout loHoule= new BuGridLayout();
    loHoule.setColumns(3);
    loHoule.setHgap(5);
    loHoule.setVgap(5);
    pnHoule.setLayout(loHoule);
    pnHoule.setBorder(new EmptyBorder(5, 5, 5, 5));
    n= 0;
    tf_periode_houle= BuTextField.createDoubleField();
    tf_hauteur_houle= BuTextField.createDoubleField();
    tf_hauteur_mer= BuTextField.createDoubleField();
    tf_incidence_houle= BuTextField.createDoubleField();
    tf_nbmaillesXI= BuTextField.createIntegerField();
    tf_nbmaillesETA= BuTextField.createIntegerField();
    tf_xd1= BuTextField.createDoubleField();
    tf_yd1= BuTextField.createDoubleField();
    tf_xd2= BuTextField.createDoubleField();
    tf_yd2= BuTextField.createDoubleField();
    tf_xd3= BuTextField.createDoubleField();
    tf_yd3= BuTextField.createDoubleField();
    tf_periode_houle.setName("tfPERIODEHOULE");
    tf_hauteur_houle.setName("tfHAUTEURHOULE");
    tf_hauteur_mer.setName("tfHAUTEURMER");
    tf_incidence_houle.setName("tfINCIDENCEHOULE");
    tf_nbmaillesXI.setName("tfNBMAILLESXI");
    tf_nbmaillesETA.setName("tfNBMAILLESETA");
    tf_xd1.setName("tfXD1");
    tf_yd1.setName("tfYD1");
    tf_xd2.setName("tfXD2");
    tf_yd2.setName("tfYD2");
    tf_xd3.setName("tfXD3");
    tf_yd3.setName("tfYD3");
    tf_periode_houle.setText("1.");
    tf_hauteur_houle.setText("0.0464");
    tf_hauteur_mer.setText("0.");
    tf_incidence_houle.setText("90.");
    tf_nbmaillesXI.setText("4");
    tf_nbmaillesETA.setText("4");
    tf_xd1.setText("0.");
    tf_yd1.setText("0.");
    tf_xd2.setText("100.");
    tf_yd2.setText("0.");
    tf_xd3.setText("20.");
    tf_yd3.setText("20.");
    tf_periode_houle.setColumns(10);
    tf_hauteur_houle.setColumns(10);
    tf_hauteur_mer.setColumns(10);
    tf_incidence_houle.setColumns(10);
    tf_nbmaillesXI.setColumns(10);
    tf_nbmaillesETA.setColumns(10);
    tf_xd1.setColumns(10);
    tf_yd1.setColumns(10);
    tf_xd2.setColumns(10);
    tf_yd2.setColumns(10);
    tf_xd3.setColumns(10);
    tf_yd3.setColumns(10);
    pnHoule.add(
      new JLabel(
        CurviResource.CURVI.getString("P�riode de la houle:"),
        SwingConstants.RIGHT),
      n++);
    pnHoule.add(tf_periode_houle, n++);
    pnHoule.add(new JLabel("s", SwingConstants.LEFT), n++);
    pnHoule.add(
      new JLabel(
        CurviResource.CURVI.getString("Hauteur de la houle:"),
        SwingConstants.RIGHT),
      n++);
    pnHoule.add(tf_hauteur_houle, n++);
    pnHoule.add(new JLabel("m", SwingConstants.LEFT), n++);
    pnHoule.add(
      new JLabel(
        CurviResource.CURVI.getString("Hauteur de la mer:"),
        SwingConstants.RIGHT),
      n++);
    pnHoule.add(tf_hauteur_mer, n++);
    pnHoule.add(new JLabel("m", SwingConstants.LEFT), n++);
    pnHoule.add(
      new JLabel(
        CurviResource.CURVI.getString("Incidence de la houle:"),
        SwingConstants.RIGHT),
      n++);
    pnHoule.add(tf_incidence_houle, n++);
    pnHoule.add(new JLabel("�", SwingConstants.LEFT), n++);
    final String entier= CurviResource.CURVI.getString("entier");
    final String reel= CurviResource.CURVI.getString("r�el");
    pnHoule.add(
      new JLabel(
        CurviResource.CURVI.getString("Mailles selon XI:"),
        SwingConstants.RIGHT),
      n++);
    pnHoule.add(tf_nbmaillesXI, n++);
    pnHoule.add(new JLabel(entier, SwingConstants.LEFT), n++);
    pnHoule.add(
      new JLabel(
        CurviResource.CURVI.getString("Mailles selon ETA:"),
        SwingConstants.RIGHT),
      n++);
    pnHoule.add(tf_nbmaillesETA, n++);
    pnHoule.add(new JLabel(entier, SwingConstants.LEFT), n++);
    pnHoule.add(new JLabel("xd1:", SwingConstants.RIGHT), n++);
    pnHoule.add(tf_xd1, n++);
    pnHoule.add(new JLabel(reel, SwingConstants.LEFT), n++);
    pnHoule.add(new JLabel("yd1:", SwingConstants.RIGHT), n++);
    pnHoule.add(tf_yd1, n++);
    pnHoule.add(new JLabel(reel, SwingConstants.LEFT), n++);
    pnHoule.add(new JLabel("xd2:", SwingConstants.RIGHT), n++);
    pnHoule.add(tf_xd2, n++);
    pnHoule.add(new JLabel(reel, SwingConstants.LEFT), n++);
    pnHoule.add(new JLabel("yd2:", SwingConstants.RIGHT), n++);
    pnHoule.add(tf_yd2, n++);
    pnHoule.add(new JLabel(reel, SwingConstants.LEFT), n++);
    pnHoule.add(new JLabel("xd3:", SwingConstants.RIGHT), n++);
    pnHoule.add(tf_xd3, n++);
    pnHoule.add(new JLabel(reel, SwingConstants.LEFT), n++);
    pnHoule.add(new JLabel("yd3:", SwingConstants.RIGHT), n++);
    pnHoule.add(tf_yd3, n++);
    pnHoule.add(new JLabel(reel, SwingConstants.LEFT), n++);
    tpMain= new JTabbedPane();
    tpMain.setName("tpPARAMETRES");
    // tpMain.setTabPlacement(SwingConstants.LEFT);
    final JPanel cdGenerales= new JPanel();
    cdGenerales.setLayout(new BuBorderLayout());
    cdGenerales.add(pnGenerales, BuBorderLayout.NORTH);
    tpMain.addTab(
      FudaaLib.getS("G�n�raux") + " ",
      BuResource.BU.getIcon("calculer"),
      cdGenerales,
      FudaaLib.getS("Param�tres g�n�raux"));
    final JPanel cdHoule= new JPanel();
    cdHoule.setLayout(new BuBorderLayout());
    cdHoule.add(pnHoule, BuBorderLayout.NORTH);
    tpMain.addTab(
      CurviResource.CURVI.getString("Houle") + " ",
      CurviResource.CURVI.getIcon("houle"),
      cdHoule,
      CurviResource.CURVI.getString("Param�tres descriptifs de la houle"));
    content_= (JComponent)getContentPane();
    content_.setLayout(new BuBorderLayout());
    content_.setBorder(new EmptyBorder(5, 5, 5, 5));
    content_.add(tpMain, BuBorderLayout.CENTER);
    setTitle(FudaaLib.getS("Param�tres"));
    setFrameIcon(BuResource.BU.getIcon("parametre"));
    setLocation(40, 40);
    pack();
  }
  public void setSelectedIndex(final int _i) {
    tpMain.setSelectedIndex(_i);
  }
  public String getNomProjet() {
    return tf_nom_projet.getText();
  }
  public SParametresCurviINP creeSParametresCurviINP() {
    SParametresCurviINP r= null;
    try {
      int ch_cas_n= ch_cas.getSelectedIndex();
      ch_cas_n--;
      final int ch_maillage_n= ch_maillage.getSelectedIndex();
      r=
        new SParametresCurviINP(
          (new Double(tf_periode_houle.getText())).doubleValue(),
          (new Double(tf_hauteur_houle.getText())).doubleValue(),
          (new Double(tf_hauteur_mer.getText())).doubleValue(),
          (new Double(tf_incidence_houle.getText())).doubleValue(),
          (new Integer(tf_nbmaillesXI.getText())).intValue(),
          (new Integer(tf_nbmaillesETA.getText())).intValue(),
          (new Double(tf_xd1.getText())).doubleValue(),
          (new Double(tf_yd1.getText())).doubleValue(),
          (new Double(tf_xd2.getText())).doubleValue(),
          (new Double(tf_yd2.getText())).doubleValue(),
          (new Double(tf_xd3.getText())).doubleValue(),
          (new Double(tf_yd3.getText())).doubleValue(),
          ch_maillage_n,
          ch_cas_n,
          (cb_reprise_vag.isSelected() ? 1 : 0));
    } catch (final Exception ex) {
      r= null;
      System.err.println("" + ex);
    }
    return r;
  }
}
