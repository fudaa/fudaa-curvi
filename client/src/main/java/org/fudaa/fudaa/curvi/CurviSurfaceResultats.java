/*
 * @file         CurviSurfaceResultats.java
 * @creation     1999-01-13
 * @modification $Date: 2006-09-19 15:02:26 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.curvi;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JComponent;

import org.fudaa.ctulu.CtuluLibString;
/**
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:02:26 $ by $Author: deniger $
 * @author       Guillaume Desnoix 
 */
public class CurviSurfaceResultats extends JComponent {
  double xmin, ymin, zmin, xmax, ymax, zmax;
  double[][] p_;
  int c_;
  public CurviSurfaceResultats(final double[][] _p, final int _c) {
    p_= _p;
    c_= _c;
    System.err.println("Calcul du domaine");
    xmax= ymax= zmax= -Double.MAX_VALUE;
    xmin= ymin= zmin= Double.MAX_VALUE;
    for (int i= 0; i < p_.length; i++) {
      if (p_[i][0] < xmin) {
        xmin= p_[i][0];
      }
      if (p_[i][0] > xmax) {
        xmax= p_[i][0];
      }
      if (p_[i][1] < ymin) {
        ymin= p_[i][1];
      }
      if (p_[i][1] > ymax) {
        ymax= p_[i][1];
      }
      if (p_[i][2] < zmin) {
        zmin= p_[i][2];
      }
      if (p_[i][2] > zmax) {
        zmax= p_[i][2];
      }
    }
    System.err.println(
      "("
        + xmin
        + CtuluLibString.VIR
        + ymin
        + CtuluLibString.VIR
        + zmin
        + ")x("
        + xmax
        + CtuluLibString.VIR
        + ymax
        + CtuluLibString.VIR
        + zmax
        + ")");
    final Dimension d= new Dimension(400, 400);
    setPreferredSize(d);
    setSize(d);
  }
  public void paint(final Graphics _g) {
    super.paint(_g);
    int x, y, z;
    int r, v, b;
    final Dimension d= getSize();
    final int w= d.width;
    final int h= d.height;
    for (int i= 0; i < p_.length; i++) {
      x= (int) ((p_[i][0] - xmin) * w / (xmax - xmin));
      y= h - (int) ((p_[i][1] - ymin) * h / (ymax - ymin));
      z= (int) ((p_[i][2] - zmin) * 255 / (zmax - zmin));
      r= v= b= 0;
      switch (c_) {
        case 0 :
          r= v= b= z;
          break;
        case 1 :
          r= z;
          break;
        case 2 :
          v= z;
          break;
        case 4 :
          b= z;
          break;
        case 3 :
          r= v= z;
          break;
        case 5 :
          r= b= z;
          break;
        case 6 :
          v= b= z;
          break;
        case 7 :
          r= v= b= z;
          break;
        default :
          r= v= b= z;
          break;
      }
      _g.setColor(new Color(r, v, b));
      _g.fillOval(x - 3, y - 3, 7, 7);
    }
  }
}
