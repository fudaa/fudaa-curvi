#!/bin/bash


for i in *.gif
do
  f=`basename $i .gif `
  cp $f.gif $f_bak.gif
  n=`webgif -l $f.gif | nawk '/192\t192\t192/ { print $1 }' `
  if [ -z "$n" ]; then
    n="-1"
  fi
  echo "$f T=$n"
  webgif -t $n $f.gif
done

for i in *.gif
do
  f=`basename $i .gif `
  echo "$f --> 24,16"
  $HOME/cgi/gd/_taille/taille.x 24 24 <$f.gif >../${f}_24.gif
  $HOME/cgi/gd/_taille/taille.x 16 16 <$f.gif >../${f}_16.gif
done